/**
 * Boolean wird auf true geschaltet wenn eine Änderung im Formular vorgenommen wird
 */
var projectChange = false;
var financeChange = false;
var timeChange = false;
var statusChange = false;
var riskChange = false;

/**
 * Beim laden des Dokuments werden die zusätzlichen Tabs versteckt, wenn ein neues Projekt erstellt wird
 */
document.addEventListener('DOMContentLoaded', function() {
  hideTabs();
  calculateCost();
  calculateTime();
  calculateOverallState();
  switchTab();
}, false);

/**
 * Tabs einzeln verstecken wenn es noch keine Projekt ID gibt
 */
function hideTabs() {
  if(document.getElementById("projectId").value == 0) {
    document.getElementById("financeTab").style.display = "none";
    document.getElementById("timeTab").style.display = "none";
    document.getElementById("statusTab").style.display = "none";
    document.getElementById("riskTab").style.display = "none";
  }
}

function switchTab(){
  var urlParamTab = document.getElementById("tabField").value;

  if(urlParamTab == "") {
    var urlParamTab = getUrlParameter("tab");
  }
 
  //Prüfen auf welchem Tab ich mich befinde
  if(urlParamTab == "finance"){
    document.getElementById("financeLi").classList.add("active");
    document.getElementById("finance").classList.add("active");
    document.getElementById("finance").classList.add("in");

  } else if(urlParamTab == "time"){
    document.getElementById("timeLi").classList.add("active");
    document.getElementById("time").classList.add("active");
    document.getElementById("time").classList.add("in");

  } else if(urlParamTab == "status"){
    document.getElementById("statusLi").classList.add("active");
    document.getElementById("status").classList.add("active");
    document.getElementById("status").classList.add("in");

  } else if(urlParamTab == "risk"){
    document.getElementById("riskLi").classList.add("active");
    document.getElementById("risk").classList.add("active");
    document.getElementById("risk").classList.add("in");

  } else {
    document.getElementById("infoLi").classList.add("active");
    document.getElementById("info").classList.add("active");
    document.getElementById("info").classList.add("in");
  }
}

function getUrlParameter(variable){
  var url = window.location.href.toString();

  if(url.split("?")[1]) {
    var allParams = url.split("?")[1].toString();
  } else {
    return null;
  }

  if(allParams.slice(-1) == "#") {
    allParams = allParams.substring(0, allParams.length-1);
  }

  var params = allParams.split("&");

  for(var i = 0; i < params.length; i++) {
    if(params[i].split("=")[0] == variable) {
      return params[i].split("=")[1];
    }
  }
}

function onProjectChange() {
  projectChange = true;
}

function onFinanceChange() {
  financeChange = true;
}

function onTimeChange() {
  timeChange = true;
}

function checkChange() {
  if(projectChange == true) {
    var addButtons = document.getElementsByClassName("new-person");
    addButtons[0].setAttribute("data-target", "#saveAlert")
    addButtons[1].setAttribute("data-target", "#saveAlert")
  }
}

function setButtonBack() {
  var addButtons = document.getElementsByClassName("new-person");
  addButtons[0].setAttribute("data-target", "#newPerson")
  addButtons[1].setAttribute("data-target", "#newPerson")
}

function tabChange(targetTab){
  var tabId = targetTab + "Tab";
  if(projectChange || financeChange || timeChange || statusChange || riskChange) {
    document.getElementById(tabId).setAttribute("data-target", "#tabChangeAlert");
    document.getElementById(tabId).setAttribute("data-toggle", "modal");
  } else {
    document.getElementById(tabId).setAttribute("href", "#" + targetTab);
    document.getElementById(tabId).setAttribute("data-toggle", "tab");
  }
}

function formSubmitCost() {
  document.getElementById("formCost").submit();
}

/**
 * Nur die befüllten Formulare auf dem Tab Time als POST versenden
 */
function formSubmitTime() {
  // Sollwerte in Variable schreiben
  var startSoll = document.getElementById("start_soll").value;
  var endSoll = document.getElementById("end_soll").value;
  var nomInitialSoll = document.getElementById("nom_initial_soll").value;
  var nomConceptionSoll = document.getElementById("nom_conception_soll").value;
  var nomRealizationSoll = document.getElementById("nom_realization_soll").value;
  var nomIntroductionSoll = document.getElementById("nom_introduction_soll").value;
  var tgtInitialSoll = document.getElementById("tgt_initial_soll").value;
  var tgtConceptionSoll = document.getElementById("tgt_conception_soll").value;
  var tgtRealizationSoll = document.getElementById("tgt_realization_soll").value;
  var tgtIntroductionSoll = document.getElementById("tgt_introduction_soll").value;

  // Ist Werte in Variabel schreiben
  var startIst = document.getElementById("start_ist").value;
  var endIst = document.getElementById("end_ist").value;
  var nomInitialIst = document.getElementById("nom_initial_ist").value;
  var nomConceptionIst = document.getElementById("nom_conception_ist").value;
  var nomRealizationIst = document.getElementById("nom_realization_ist").value;
  var nomIntroductionIst = document.getElementById("nom_introduction_ist").value;
  var tgtInitialIst = document.getElementById("tgt_initial_ist").value;
  var tgtConceptionIst = document.getElementById("tgt_conception_ist").value;
  var tgtRealizationIst = document.getElementById("tgt_realization_ist").value;
  var tgtIntroductionIst = document.getElementById("tgt_introduction_ist").value;

  // Prüfen ob Input Felder leer sind, falls ja mit Wert NULL befüllen
  if (startSoll == "") {
    document.getElementById("start_soll").disabled;
  }
  if (endSoll == "") {
    document.getElementById("end_soll").disabled;
  }
  if (nomInitialSoll == "") {
	document.getElementById("nom_initial_soll").disabled;
  }
  if (nomConceptionSoll == "") {
	document.getElementById("nom_conception_soll").disabled;
  }
  if (nomRealizationSoll == "") {
	document.getElementById("nom_realization_soll").disabled;
  }
  if (nomIntroductionSoll == "") {
	document.getElementById("nom_introduction_soll").disabled;
  }
  if (tgtInitialSoll == "") {
	document.getElementById("tgt_initial_soll").disabled;
  }
  if (tgtConceptionSoll == "") {
	document.getElementById("tgt_conception_soll").disabled;
  }
  if (tgtRealizationSoll == "") {
	document.getElementById("tgt_realization_soll").disabled;
  }
  if (tgtIntroductionSoll == "") {
	document.getElementById("tgt_introduction_soll").disabled;
  }
	
  if (startIst == "") {
	document.getElementById("start_ist").disabled;
  }
  if (endIst == "") {
	document.getElementById("end_ist").disabled;
  }
  if (nomInitialIst == "") {
	document.getElementById("nom_initial_ist").disabled;
  }
  if (nomConceptionIst == "") {
    document.getElementById("nom_conception_ist").disabled;
  }
  if (nomRealizationIst == "") {
	document.getElementById("nom_realization_ist").disabled;
  }
  if (nomIntroductionIst == "") {
	document.getElementById("nom_introduction_ist").disabled;
  }
  if (tgtInitialIst == "") {
	document.getElementById("tgt_initial_ist").disabled;
  }
  if (tgtConceptionIst == "") {
	document.getElementById("tgt_conception_ist").disabled;
  }
  if (tgtRealizationIst == "") {
	document.getElementById("tgt_realization_ist").disabled;
  }
  if (tgtIntroductionIst == "") {
	document.getElementById("tgt_introduction_ist").disabled;
  }
  disableEmptyInputs(document.getElementById("formTime"))
  document.getElementById("formTime").submit();
}

function disableEmptyInputs(form) {
  var controls = form.elements;
  for (var i=0, iLen=controls.length; i<iLen; i++) {
    controls[i].disabled = controls[i].value == '';
  }
}

/**
 * Validierung Projektformular
 */
function validateAndSubmitProject(){
  // Feldwerte aus dem Formular laden
  var name = document.getElementById("name").value;
  var start = document.getElementById("start").value;
  var end = document.getElementById("end").value;
  // Regex Pattern für Namensvalidierung
  var regex = /^[\\p{L}0-9a-zA-ZäöüÄÖÜéèà _:.,]*$/;
	
  // Prüfen ob Projektname nicht leer ist
  if (name == null || name == "") {
    alert("Projektname darf nicht leer sein");
    return false;
  }

  // Prüfen ob Projektname nicht mehr als 50 Zeichen
  if (name.length > 50) {
	  alert("Projektname darf nicht mehr als 50 Zeichen sein");
	    return false;
  }
  
  // Zeichen in Vorname prüfen
  if(!regex.test(name)) {
    alert("Der Projektname darf nur folgende Zeichen beinhalten: A-Z, 0-9, Leerzeichen und _:.,");
    return false;
  }
  
  // Prüfen ob Projekt Start nicht leer ist
  if (start == null || start == "") {
    alert("Projekt Start darf nicht leer sein");
    return false;
  }

  // Prüfen ob Projekt Ende leer ist
  if (end == null || end == "") {
    alert("Projekt Ende darf nicht leer sein");
    return false;
  }
  
  // Prüfn ob Projekt Start nicht vor dem Projekt Ende liegt
  if(Date.parse(end) - Date.parse(start) < 0) {
	  alert("Projekt Ende darf nicht vor dem Projekt Start liegen");
	  return false; 
  }

  // Formular absenden
  document.forms["Project"].submit();
}

/**
 * Validierung Personenformular
 */
function validateAndSubmitPeople(){
  // Feldwerte aus dem Formular laden
  var firstname = document.getElementById("peopleFirstname").value;
  var lastname = document.getElementById("peopleLastname").value;
  // Regex Pattern für Namensvalidierung
  var regex = /^[\\p{L}0-9a-zA-ZäöüÄÖÜéèà ]*$/;
	
  // Prüfen ob Vorname nicht leer ist
  if (firstname == null || firstname == "") {
    alert("Vorname darf nicht leer sein");
    return false;
  }

  // Prüfen ob Vorname nicht mehr als 25 Zeichen
  if (firstname.length > 25) {
	  alert("Vorname darf nicht mehr als 25 Zeichen sein");
	    return false;
  }
  
  // Zeichen in Vorname prüfen
  if(!regex.test(firstname)) {
    alert("Der Vorname darf nur folgende Zeichen beinhalten: A-Z, 0-9 und Leerzeichen");
    return false;
  }

  // Prüfen ob Nachname leer ist
  if (lastname == null || lastname == "") {
    alert("Nachname darf nicht leer sein");
    return false;
  }
  
  // Prüfen ob Nachname nicht mehr als 25 Zeichen
  if (lastname.length > 25) {
	  alert("Nachname darf nicht mehr als 25 Zeichen sein");
	    return false;
  }

  // Zeichen in Nachname prüfen
  if(!regex.test(lastname)) {
    alert("Der Nachname darf nur folgende Zeichen beinhalten: A-Z, 0-9 und Leerzeichen");
    return false;
  }

  // Formular absenden
  document.forms["People"].submit();
}

/**
 * Validierung Risk Formular
 */
function validateAndSubmitRisk(){
  // Feldwerte aus dem Formular laden
  var description = document.getElementById("decription").value;
  var measure = document.getElementById("measures").value;
  var etw = document.getElementById("etw").value;	
  // Regex Pattern für Fliestext
  var regex = /^[\\p{L}0-9a-zA-ZäöüÄÖÜ_ ]*$/;
	
  // Prüfen ob Beschreibung nicht leer ist
  if (description == null || description == "") {
    alert("Beschreibung darf nicht leer sein");
    return false;
  }

  // Zeichen in Beschreibung prüfen
  if(!regex.test(description)) {
    alert("Die Beschreibung darf nur folgende Zeichen beinhalten: a-Z, 0-9, Leerzeichen und :.,-?!");
    return false;
  }
  
  //Prüfen ob Beschreibung nicht mehr als 250 Zeichen ist
  if (description.length > 250) {
	alert("Beschreibung darf nicht mehr als 250 Zeichen sein");
	return false;
  }

  // Prüfen ob Masnahme leer ist
  if (measure == null || measure == "") {
    alert("Masnahme darf nicht leer sein");
    return false;
  }
  
  //Prüfen ob Masnahme nicht mehr als 250 Zeichen ist
  if (measure.length > 250) {
	alert("Masnahme darf nicht mehr als 250 Zeichen sein");
	return false;
  }

  // Zeichen in Masnahme prüfen
  if(!regex.test(measure)) {
    alert("Die Masnahme darf nur folgende Zeichen beinhalten: a-Z, 0-9, Leerzeichen und :.,-?!");
    return false;
  }
  
  // Prüfen ob ETW leer ist
  if (etw == null || etw == "") {
    alert("ETW darf nicht leer sein");
    return false;
  }
  
  //Prüfen ob ETW nicht grösser als 100 ist
  if (etw > 100) {
	alert("ETW kann nicht höher als 100% sein");
	return false;
  }
  
  // Prüfen ob ETW ein Float Wert ist
  if(isNaN(etw)){
	  alert("Die Eintrittswahrscheinlichkeit muss eine Fliesskommazahl sein");
	  return false;  
  }

  // Formular absenden
  document.getElementById("formRisk").submit();
}

/**
 * Validierung Status Formular
 */
function validateAndSubmitState(){
  // Feldwerte aus dem Formular laden
  var period_start = document.getElementById("period_start").value;
  var period_end = document.getElementById("period_end").value;
  var comment = document.getElementById("comment").value;	
  // Regex Pattern für Fliestext
  var regex = /^[\\p{L}0-9a-zA-ZäöüÄÖÜ_ :.,-?!]*$/;
	
  // Prüfen ob Startdatum nicht leer ist
  if (period_start == null || period_start == "") {
    alert("Startdatum darf nicht leer sein");
    return false;
  }

  // Prüfen ob Enddatum leer ist
  if (period_end == null || period_end == "") {
    alert("Enddatum darf nicht leer sein");
    return false;
  }
  
  // Prüfn ob Startdatum nicht vor dem Enddatum liegt
  if(Date.parse(period_end) - Date.parse(period_start) < 0) {
	  alert("Enddatum darf nicht vor dem Startdatum liegen");
	  return false; 
  }

  // Prüfen ob Kommentar leer ist
  if (comment == null || comment == "") {
    alert("Kommentar darf nicht leer sein");
    return false;
  }
  
  //Prüfen ob Kommentar nicht mehr als 250 Zeichen ist
  if (comment.length > 250) {
	alert("Kommentar darf nicht mehr als 250 Zeichen sein");
	return false;
  }
  
  // Zeichen in Kommentar prüfen
  if(!regex.test(comment)) {
    alert("Der Kommentar darf nur folgende Zeichen beinhalten: a-Z, 0-9, Leerzeichen und :.,-?!");
    return false;
  }

  // Formular absenden
  document.getElementById("formState").submit();
}

/**
 * Werte für Cost in Variablen Speichern und Delta berrechnen
 * Speichert Soll- und Ist-Werte in Variabeln und berechnet Totalwert und Delta
 */
function calculateCost() {
  // Soll- und Ist-Werte in Variabeln schreiben
  var nomInitial = parseFloat(document.getElementById("nom_initial").value);
  var nomConception = parseFloat(document.getElementById("nom_conception").value);
  var nomRealization = parseFloat(document.getElementById("nom_realization").value);
  var nomIntroduction = parseFloat(document.getElementById("nom_introduction").value);
  var tgtInitial = parseFloat(document.getElementById("tgt_initial").value);
  var tgtConception = parseFloat(document.getElementById("tgt_conception").value);
  var tgtRealization = parseFloat(document.getElementById("tgt_realization").value);
  var tgtIntroduction = parseFloat(document.getElementById("tgt_introduction").value);

  var progress;
  if (document.getElementById('progress') != null) {
    progress = parseInt(document.getElementById("progress").value);
  }
  
  // Total und Delta berechnen
  var financeTotalShould = nomInitial + nomConception + nomRealization + nomIntroduction;

  document.getElementById("financeTotalShould").innerHTML = nomInitial + nomConception + nomRealization + nomIntroduction;
  document.getElementById("financeTotalIs").innerHTML = tgtInitial + tgtConception + tgtRealization + tgtIntroduction;

  document.getElementById("financeInitDelta").innerHTML = nomInitial - tgtInitial;
  document.getElementById("financeConceptDelta").innerHTML = nomConception - tgtConception;
  document.getElementById("financeRealDelta").innerHTML = nomRealization - tgtRealization;
  document.getElementById("financeIntroDelta").innerHTML = nomIntroduction - tgtIntroduction;
  document.getElementById("financeTotalDelta").innerHTML = nomInitial + nomConception + nomRealization + nomIntroduction - tgtInitial - tgtConception - tgtRealization - tgtIntroduction;

  if (progress && financeTotalShould) {
    document.getElementById("calculatedEV").innerHTML = (financeTotalShould * progress) / 100;
  }
}

/**
 * Werte für Time in Variablen Speichern und Delta berrechnen
 */
function calculateTime() {
  // Soll- und Ist-Werte in Variabeln schreiben
  var startSoll = String(document.getElementById("start_soll").value);
  var endSoll = String(document.getElementById("end_soll").value);
  var nomInitialSoll = String(document.getElementById("nom_initial_soll").value);
  var nomConceptionSoll = String(document.getElementById("nom_conception_soll").value);
  var nomRealizationSoll = String(document.getElementById("nom_realization_soll").value);
  var nomIntroductionSoll = String(document.getElementById("nom_introduction_soll").value);
  var tgtInitialSoll = String(document.getElementById("tgt_initial_soll").value);
  var tgtConceptionSoll = String(document.getElementById("tgt_conception_soll").value);
  var tgtRealizationSoll = String(document.getElementById("tgt_realization_soll").value);
  var tgtIntroductionSoll = String(document.getElementById("tgt_introduction_soll").value);
  var startIst = String(document.getElementById("start_ist").value);
  var endIst = String(document.getElementById("end_ist").value);
  var nomInitialIst = String(document.getElementById("nom_initial_ist").value);
  var nomConceptionIst = String(document.getElementById("nom_conception_ist").value);
  var nomRealizationIst = String(document.getElementById("nom_realization_ist").value);
  var nomIntroductionIst = String(document.getElementById("nom_introduction_ist").value);
  var tgtInitialIst = String(document.getElementById("tgt_initial_ist").value);
  var tgtConceptionIst = String(document.getElementById("tgt_conception_ist").value);
  var tgtRealizationIst = String(document.getElementById("tgt_realization_ist").value);
  var tgtIntroductionIst = String(document.getElementById("tgt_introduction_ist").value);

  // Delta berechnen
  if (startSoll && startIst) {
    document.getElementById("timeStartDelta").innerHTML = calculateDifferenceInDays(startIst,startSoll);
  }
  if (endSoll && endIst) {
    document.getElementById("timeEndDelta").innerHTML = calculateDifferenceInDays(endIst,endSoll);
  }
  if (nomInitialSoll && nomInitialIst) {
    document.getElementById("timeNomInitialDelta").innerHTML = calculateDifferenceInDays(nomInitialIst,nomInitialSoll);
  }
  if (nomConceptionSoll && nomConceptionIst) {
    document.getElementById("timeNomConceptionDelta").innerHTML = calculateDifferenceInDays(nomConceptionIst,nomConceptionSoll);
  }
  if (nomRealizationSoll && nomRealizationIst) {
    document.getElementById("timeNomRealizationDelta").innerHTML = calculateDifferenceInDays(nomRealizationIst,nomRealizationSoll);
  }
  if (nomIntroductionSoll && nomIntroductionIst) {
    document.getElementById("timeNomIntroductionDelta").innerHTML = calculateDifferenceInDays(nomIntroductionIst,nomIntroductionSoll);
  }
  if (tgtInitialSoll && tgtInitialIst) {
    document.getElementById("timeTgtInitialDelta").innerHTML = calculateDifferenceInDays(tgtInitialIst,tgtInitialSoll);
  }
  if (tgtConceptionSoll && tgtConceptionIst) {
    document.getElementById("timeTgtConceptionDelta").innerHTML = calculateDifferenceInDays(tgtConceptionIst,tgtConceptionSoll);
  }
  if (tgtRealizationSoll && tgtRealizationIst) {
    document.getElementById("timeTgtRealizationDelta").innerHTML = calculateDifferenceInDays(tgtRealizationIst,tgtRealizationSoll);
  }
  if (tgtIntroductionSoll && tgtIntroductionIst) {
    document.getElementById("timeTgtIntroductionDelta").innerHTML = calculateDifferenceInDays(tgtIntroductionIst,tgtIntroductionSoll);
  }
}

/**
 * Delta von zwei Daten ausrechnen inkl. Format umwandeln
 */
function calculateDifferenceInDays(date1, date2) {
  var startDateArr=date1.split('-'); // Date as 2016-05-18
  var endDateArr=date2.split('-');
  var d0 = new Date(startDateArr[0],startDateArr[1],startDateArr[2]);
  var d1 = new Date(endDateArr[0],endDateArr[1],endDateArr[2]);

  var DayDiffToReturn = daysDifference(d0,d1);
  var textToReturn = "Tage";
  
  if ( (DayDiffToReturn == "1") || (DayDiffToReturn == "-1") ) {
	  textToReturn = "Tag";
  }
  return DayDiffToReturn + " " + textToReturn;
}

/**
 *  Berechnet Differenz von zwei Daten
 *  https://stackoverflow.com/questions/7763327/how-to-calculate-date-difference-in-javascript
 */
function daysDifference(d0, d1) {
  var diff = new Date(+d1).setHours(12) - new Date(+d0).setHours(12);
  return Math.round(diff/8.64e7);
}

/**
 * Overall Status berechnen und div id setzen
 */
function calculateOverallState(){
	var overallStateTds = document.getElementsByClassName("overallStates");
	
	for(var i = 0; i < overallStateTds.length; i++) {
		var stateId = overallStateTds[i].id.substr(12);
		
		var stateCost = document.getElementById("stateCost" + stateId).className;
		var stateDate = document.getElementById("stateDate" + stateId).className;
		var stateRessource = document.getElementById("stateRessource" + stateId).className;
		var stateRisk = document.getElementById("stateRisk" + stateId).className;
		
		var countYellow = 0;
		var countRed = 0;
		
		switch (stateCost) {
		  case "gelb":
			  countYellow++;
			  break;
		  case "rot":
			  countRed++;
			  break;
		}
		
		switch (stateDate) {
		  case "gelb":
			  countYellow++;
			  break;
		  case "rot":
			  countRed++;
			  break;
		}
		
		switch (stateRessource) {
		  case "gelb":
			  countYellow++;
			  break;
		  case "rot":
			  countRed++;
			  break;
		}
		
		switch (stateRisk) {
		  case "gelb":
			  countYellow++;
			  break;
		  case "rot":
			  countRed++;
			  break;
		}
		
		if(countRed > 0){
			document.getElementById("stateOverallDiv" + stateId).className = "rot";
		} else if(countYellow > 1){
			document.getElementById("stateOverallDiv" + stateId).className = "gelb";
		} else {
			document.getElementById("stateOverallDiv" + stateId).className = "gruen";
		}
	}
}
