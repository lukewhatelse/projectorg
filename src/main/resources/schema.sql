/* Auskommentiert, da nicht jedesmal die Tabelle gelöscht werden müssen

--
-- Drop tables for PPP Tool
--
DROP TABLE IF EXISTS `time`;
DROP TABLE IF EXISTS `risk`;
DROP TABLE IF EXISTS `cost`;
DROP TABLE IF EXISTS `state`;
DROP TABLE IF EXISTS `project`;
DROP TABLE IF EXISTS `people`;
DROP TABLE IF EXISTS `items`;

*/

--
-- Create tables for PPP Tool
--
CREATE TABLE IF NOT EXISTS `people` (
	`id` int NOT NULL AUTO_INCREMENT,
	`firstname` varchar(25) NOT NULL,
	`lastname` varchar(25) NOT NULL,
	PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS `project` (
	`id` int NOT NULL AUTO_INCREMENT,
	`name` varchar(50) NOT NULL,
	`start` date NOT NULL,
	`end` date NOT NULL,
	`phase` varchar(20),
	`customer_id` int,
	`projectleader_id` int,
	PRIMARY KEY (id),
	CONSTRAINT fk_peopleProject_customer FOREIGN KEY (customer_id) REFERENCES people(id),
	CONSTRAINT fk_peopleProject_projectleader FOREIGN KEY (projectleader_id) REFERENCES people(id)
);

CREATE TABLE IF NOT EXISTS `cost` (
	`id` int NOT NULL AUTO_INCREMENT,
	`project_id` int NOT NULL,
	`nom_initial` numeric(15,2),
	`nom_conception` numeric(15,2),
	`nom_realization` numeric(15,2),
	`nom_introduction` numeric(15,2),
	`tgt_initial` numeric(15,2),
	`tgt_conception` numeric(15,2),
	`tgt_realization` numeric(15,2),
	`tgt_introduction` numeric(15,2),
	PRIMARY KEY (id),
	CONSTRAINT fk_projectCost FOREIGN KEY (project_id) REFERENCES project(id)
);

CREATE TABLE IF NOT EXISTS `state` (
	`id` int NOT NULL AUTO_INCREMENT,
	`project_id` int NOT NULL,
	`period_start` date,
	`period_end` date,
	`cost` int,
	`date` int,
	`ressource` int,
	`risk` int,
	`progress` int,
	`comment` varchar(250),
	PRIMARY KEY (id),
	CONSTRAINT fk_projectState FOREIGN KEY (project_id) REFERENCES project(id)
);

CREATE TABLE IF NOT EXISTS `risk` (
	`id` int NOT NULL AUTO_INCREMENT,
	`project_id` int NOT NULL,
	`description` varchar(250),
	`measures` varchar(250),
	`etw` float,
	`impact` int,
	`critical` int,
	`responsible_id` int,
	`phase` varchar(20),
	`last_edited` date,
	PRIMARY KEY (id),
	CONSTRAINT fk_projectRisk FOREIGN KEY (project_id) REFERENCES project(id)
);

CREATE TABLE IF NOT EXISTS `time` (
	`id` int NOT NULL AUTO_INCREMENT,
	`project_id` int NOT NULL,
	`start_ist` date,
	`start_soll` date,
	`end_ist` date,
	`end_soll` date,
	`nom_initial_ist` date,
	`nom_initial_soll` date,
	`nom_conception_ist` date,
	`nom_conception_soll` date,
	`nom_realization_ist` date,
	`nom_realization_soll` date,
	`nom_introduction_ist` date,
	`nom_introduction_soll` date,
	`tgt_initial_ist` date,
	`tgt_initial_soll` date,
	`tgt_conception_ist` date,
	`tgt_conception_soll` date,
	`tgt_realization_ist` date,
	`tgt_realization_soll` date,
	`tgt_introduction_ist` date,
	`tgt_introduction_soll` date,
	PRIMARY KEY (id),
	CONSTRAINT fk_projectTime FOREIGN KEY (project_id) REFERENCES project(id)
);

/* Auskommentiert, da nicht jedesmal eingelesen werden muss

--
-- testdata to import into database 
--
INSERT INTO `people`
VALUES (NULL, 'Lucas','Badertscher'),
       (NULL, 'Jürg','Wüst'),
       (NULL, 'Robin','Csontos'),
       (NULL, 'Marc','Stuber'),
       (NULL, 'Fabian','Fischer');

INSERT INTO `project`
VALUES (NULL, 'ERP Migration', '2016-03-14', '2016-10-01', 'Initialisierung', '1', '2'),
       (NULL, 'Neues PPP Tool', '2016-01-01', '2016-11-01', 'Realisierung', '3', '4');

INSERT INTO `cost`
VALUES (NULL, '1', '1000', '5000', '6000', '10000', '500', NULL, NULL, NULL),
       (NULL, '2', '500', '2000', '5000', '6000', '1000', '1500', NULL, NULL);

INSERT INTO `state`
VALUES (NULL, '1', '2016-03-15', '2016-04-15', '1', '1', '1', '1', '50', 'Projekt konnte erfolgreich gestartet werden.'),
	   (NULL, '1', '2016-03-15', '2016-04-15', '1', '1', '1', '1', '60', 'Zeitplan kann eingehalten werden'),
       (NULL, '2', '2016-03-15', '2016-06-01', '1', '2', '1', '2', '10', 'Projekt ist on track');

INSERT INTO `risk`
VALUES (NULL, '1', 'Stammdaten könne nicht übernomme werden', 'Stammdaten prüfen lassen', '10.01', '1', '1', '1', 'Initialisierung', NULL),
       (NULL, '2', 'Ressourcen sind nicht verfügbar', 'Überstunden leisten', '20', '1', '1', '3', 'Realisierung', NULL);

INSERT INTO `time`
VALUES (NULL, '1', '2016-03-14', '2016-03-14', NULL, '2016-10-01', '2016-03-14', '2016-03-14', NULL, '2016-03-31', NULL, '2016-04-30', NULL, '2016-09-01', NULL, '2016-03-31', NULL, '2016-04-30', NULL, '2016-09-01', NULL, '2016-10-01'),
       (NULL, '2', '2016-01-01', '2016-01-01', NULL, '2016-11-01', '2016-01-01', '2016-01-01', '2016-03-31', '2016-03-31', '2016-04-30', '2016-04-30', NULL, '2016-09-30', '2016-03-31', '2016-03-31', '2016-04-30', '2016-04-30', NULL, '2016-09-30', NULL, '2016-11-01');

*/