package ch.bfh.milklist.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import ch.bfh.milklist.domain.ProjectRepository;

/**
 * WWW-Controller
 * Liefert unter "/" die ganze Liste
 * 
 * @author Lucas Badertscher
 *
 */
@Controller
public class ListManagementRootController {
	
	private final Logger logger = LoggerFactory.getLogger(ListManagementRootController.class);

	/**
	 * durch das Springframework initialisiert
	 */
	@Autowired
	ProjectRepository repository;

	/**
	 * RequestMapping /: Liefert die Liste als Root-Seite "/" zurück 
	 * @param model enthält die Liste aller Items in der Variable "list"
	 * @return
	 */
	@RequestMapping("/")
	public String getList(Model model)
	{
		
		logger.trace("/");
		// Items werden geladen und die Collection dann für das Template unter dem namen "list" bereitgestellt
		// Das Template muss dann auch den Namen "list" verwenden.
		model.addAttribute("list", repository.getAll());
		return "listTemplate";
	}

}