package ch.bfh.milklist.web;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.validation.BindingResult;
import ch.bfh.milklist.domain.Project;
import ch.bfh.milklist.domain.ProjectRepository;
import ch.bfh.milklist.domain.People;
import ch.bfh.milklist.domain.PeopleRepository;
import ch.bfh.milklist.domain.Cost;
import ch.bfh.milklist.domain.CostRepository;
import ch.bfh.milklist.domain.Risk;
import ch.bfh.milklist.domain.RiskRepository;
import ch.bfh.milklist.domain.State;
import ch.bfh.milklist.domain.StateRepository;
import ch.bfh.milklist.domain.Time;
import ch.bfh.milklist.domain.TimeRepository;

import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import javax.validation.Valid;

/**
 * Controller: für alle Operationen auf einzelnen Items
 * !!! Diese Version verfügt bewusst über keine Validierung / Fehlerbehandlung !!!
 * @author Marcel Briggen
 *
 */
@Controller
public class ItemController {
	
	private final Logger logger = LoggerFactory.getLogger(ItemController.class);
	private int projectId;
	/**
	* Autowired: --> Wird durch das Springframework initialisiert (nach Typ oder Variablenname)
	*/
	@Autowired
	private ProjectRepository itemRepo;
	@Autowired
	private PeopleRepository peopleRepo;
	@Autowired
	private CostRepository costRepo;
	@Autowired
	private TimeRepository timeRepo;
	@Autowired
	private StateRepository stateRepo;
	@Autowired
	private RiskRepository riskRepo;
	@Autowired
	ProjectRepository repository;
	
	
	/**
	* ModelAttribute: Für jede Klasse die von diesem Controller aus Formulardaten gefüllt werden muss, muss eine Factorymethode erstellt werden
	* Das erzeugte Objekt wird dann abgefüllt
	* ModelAttribute --> Muss auf den im HTML-Formular (bzw. -Template) verwendeten Namen lauten
	*/
	@ModelAttribute("itemDetail")
	public Project createItem() {
		return new Project();
	}
	@ModelAttribute("peopleDetail")
	public People createPeople() {
		return new People();
	}
	@ModelAttribute("itemDetailCost")
	public Cost createCost() {
		return new Cost();
	}
	@ModelAttribute("itemDetailTime")
	public Time createTime() {
		return new Time();
	}
	@ModelAttribute("itemDetailState")
	public State createState() {
		return new State();
	}
	@ModelAttribute("itemDetailRisk")
	public Risk createRisk() {
		return new Risk();
	}

	
	/**
	 * RequestMapping /overview: Anzeige der Startseite mit dem Project Overview.
	 * @param model
	 * @return listTemplate
	 */
	@RequestMapping(method=GET, value="/overview")
	String getList(Model model)
	{
		logger.trace("/overview");
		
		// Items werden geladen und die Collection dann für das Template unter dem namen "list" bereitgestellt
		// Das Template muss dann auch den Namen "list" verwenden.
		model.addAttribute("list", repository.getAll());
		return "listTemplate";
	}
	
	
	/**
	 * RequestMapping /item: Requesthandler zum Bearbeiten eines Items.
	 * Liefert das Formular (bzw. Template) zum bearbeiten der einzelnen Felder
	 * Wenn der id Parameter 0 ist wird beim submitten des Formulars ein neues Item erstellt (Aufruf von /item/new)
	 * Wenn der id Parameter <> 0 ist wird beim submitten des Formulars das Item mit der übergebenen id upgedated (Aufruf /item/save)
	 * Hört auf GET /item
	 * @param model (Java --> Template) Hier werden die Werte vom Controller an das Template übergeben und dann durch die Tyhmeleaf-Engine zu HTML umgewandelt
	 * @param id    (WWW --> Java) Wenn id = 0 dann zeigt der Submitbutton auf "INSERT" ansonsten auf "UPDATE"
	 * @return gibt den Namen des zu verwendenden Templates zurück. Immer "itemDetailTemplate" .
	 */
	@RequestMapping(method=GET, value="/item")
	String editItem(Model model, @RequestParam(required=false) Integer id)
	{
		if(id == null)
		{
			logger.trace("GET /item für INSERT mit id " + id);
			//der Submit-Button ruft /item/new auf --> INSERT
			model.addAttribute("postAction", "/item/new");
			model.addAttribute("postActionCost", "/item/newCost");
			model.addAttribute("postActionTime", "/item/newTime");
			model.addAttribute("postActionState", "/item/newState");
			model.addAttribute("postActionRisk", "/item/newRisk");
		}
		else
		{
			this.projectId = id;
			logger.trace("GET /item für UPDATE mit id " + id);
			//der Submit-Button ruft /item/update auf --> UPDATE
			model.addAttribute("postAction", "/item/update");
			model.addAttribute("postActionCost", "/item/updateCost");
			model.addAttribute("postActionTime", "/item/updateTime");
			model.addAttribute("postActionState", "/item/newState");
			model.addAttribute("postActionRisk", "/item/newRisk");
			
			/**
			* damit die bereits in der Datenbank vorhandenen Werte im Formular gezeigt werden wird es geladen und dann
			* dem Modell unter dem Namen "itemDetail" hinzugefügt. itemDetal muss dem im HTML-Template verwendeten Namen entsprechen 
			*/
			Project pr = itemRepo.getById(projectId);
			Cost c = costRepo.getById(projectId);
			Time t = timeRepo.getById(projectId);
			State s = stateRepo.getById(projectId);
			Risk r = riskRepo.getById(projectId);
			
			model.addAttribute("itemDetail", pr);
			model.addAttribute("itemDetailCost", c);
			model.addAttribute("itemDetailTime", t);
			model.addAttribute("itemDetailState", s);
			model.addAttribute("listState", stateRepo.getListById(projectId));
			model.addAttribute("itemDetailRisk", r);
			model.addAttribute("listRisk", riskRepo.getListById(projectId));
			
			
		}
		
		State progress = stateRepo.getLatestProgressById(projectId);
		model.addAttribute("itemDetailProgress", progress);
		
		setPeopleControls(model);
		
		// Das Template itemDetail verwenden und dann "anzeigen".
		return "itemDetailTemplate";
	}
	

	/**
	 * RequestMapping /item/new: Erstellt ein neues Item in der DB. Die id wird von der Datenbank erstellt.
	 * Bei Erfolg wird wieder auf die Detailseite redirected (z.B.: /item&id=99  wenn die id 99 war.)
	 * 
	 * Hört auf POST /item/new
	 * 
	 * @param parameters (Java --> Redirect) Hier wird die ID als Parameter nach einem erfolgreichen Update im Feld "id" angehängt
	 * @param itemDetail (WWW --> Java) Das zu erstellende Item wird hier vom Formular zurückgegeben
	 * @return Redirect zurück zur Detailmaske
	 */
	@RequestMapping(method=POST, path="/item/new")
	String newItem(Model model, RedirectAttributes parameters, @ModelAttribute("itemDetail") @Valid Project itemDetail, BindingResult bindingResult)
	{
		logger.trace("POST /item/new mit itemDetail " + itemDetail);
		logger.trace("bindingResult=" + bindingResult);
		
		if (itemDetail.getStart().after(itemDetail.getEnd())){
			logger.trace("1");
			bindingResult.rejectValue("end", "error.Project","Das Enddatum muss nach dem Startdatum sein");
			logger.trace("2");
		}
		
		if (bindingResult.hasErrors()) {
			logger.trace("3");
			 model.addAttribute("postAction", "/item/new");
			 setPeopleControls(model);
			 return "itemDetailTemplate";
		}
		logger.trace("4");
		
		// insert gibt die von der DB erstellte id zurück.
		Integer id = itemRepo.insert(itemDetail);
		
		// die neue Id wird dem Redirect als Parameter hinzugefügt der redirect erfolgt dann auf /item&id=x
		parameters.addAttribute("id", id);
		return "redirect:/item";
	}
	
	
	/**
	 * RequestMapping /item/update: Schreibt das geänderte Item zurück in die Datenbank.
	 * Bei Erfolg erfolgt ein REDIRECT zurück auf die Detailseite (/item) mit der Item-id als Parameter mit dem namen id.
	 * Validierung: Im Fehlerfall wird eine durch Spring eine Fehlerseite generiert.
	 * 
	 * Hört auf POST /item/update
	 * 
	 * @param parameters (Java --> Redirect) Hier werden die Parameter für den Redirect nach einem erfolgreichen Update übergeben: die id des bearbeiteten Items 
	 * @param itemDetail (WWW --> Java) Hier kommen die vom Formular submitteten daten zurück.
	 * @return redirect nach /item: via Browser wird /item aufgerufen, also editItem weiter oben und dann das Detailformular angezeigt.
	 */
	@RequestMapping(method=POST, value="/item/update")
	String updateItem(Model model, RedirectAttributes parameters, @ModelAttribute("itemDetail") @Valid Project itemDetail, BindingResult bindingResult)
	{
		logger.trace("POST /item/update mit itemDetail " + itemDetail);
		
		if (itemDetail.getStart().after(itemDetail.getEnd())){
			bindingResult.rejectValue("end", "error.Project","Das Enddatum muss nach dem Startdatum sein");
		}
		logger.trace("bindingResult " + bindingResult);
		if (bindingResult.hasErrors()) {
			 logger.trace("Parameter " + parameters);
			 model.addAttribute("postAction", "/item/update");
			 State progress = stateRepo.getLatestProgressById(projectId);
			 model.addAttribute("itemDetailProgress", progress);
			setPeopleControls(model);
			return "itemDetailTemplate";	 
		}
		
		// Speichern des Items in dann den Parameter für den Redirect abfüllen der Redirect erfolgt dann z.B. auf /item&id=3 (wenn itemDetail.getId == 3 war)
		itemRepo.save(itemDetail);
		parameters.addAttribute("id", itemDetail.getId());
		
		return "redirect:/item";
	}
	
	
	/**
	 * RequestMapping /item/delete: Löscht das Item mit der übergebenen id in der Datenbank.
	 * /item/delete&id=987 löscht das Item mit der Id 987 aus der Datenbank
	 * 
	 * Hört auf GET /item/delete (besser wäre POST)
	 * 
	 * @param parameters (Java --> Redirect) Hier nicht verwendet da die Liste ohne Parameter aufgerufen wird
	 * @param id (WWW --> Java) Id des Items welches gelöscht werden soll.
	 * @return Redirect zurück zur Liste
	 */
	@RequestMapping(path="/item/delete")
	String deleteItem(RedirectAttributes parameters, @RequestParam Integer id)
	{
		logger.trace("GET /item/delete mit id " + id);
		
		itemRepo.delete(id);
		
		return "redirect:/";
	}
	
	
	/**
	 * RequestMapping /item/newPeople: Erstellt eine neue Person, welche als Projektleiter oder Auftraggeber ausgewählt werden kann.
	 * @param model
	 * @param parameters
	 * @param peopleDetail
	 * @param bindingResult
	 * @return
	 */
	@RequestMapping(method=POST, path="/item/newPeople")
	String newPeople(Model model, RedirectAttributes parameters, @Valid People peopleDetail, BindingResult bindingResult)
	{
		logger.trace("POST /item/newPeople mit peopleDetail " + peopleDetail);
		
		if (bindingResult.hasErrors()) {
			 return "itemDetailTemplate";
		}
		
		// insert der neu erstellten Person
		peopleRepo.insert(peopleDetail);
		
		// die neue Id wird dem Redirect als Parameter hinzugefügt der redirect erfolgt dann auf /item&id=x
		if(this.projectId != 0){
			parameters.addAttribute("id", this.projectId);
		}
		return "redirect:/item";
	}
	
	
	/**
	 * RequestMapping /item/updateCost: Erstellt ein neues Cost Item in der DB. Die id wird von der Datenbank erstellt.
	 * Bei Erfolg wird wieder auf die itemseite redirected (z.B.: /item&id=99  wenn die id 99 war.)
	 * 
	 * Hört auf POST /item/updateCost
	 * 
	 * @param model
	 * @param parameters
	 * @param itemDetailCost
	 * @param bindingResult
	 * @return Redirect auf /item
	 */
	@RequestMapping(method=POST, path="/item/updateCost")
	String newCost(Model model, RedirectAttributes parameters, @ModelAttribute("itemDetailCost") @Valid Cost itemDetailCost, BindingResult bindingResult)
	{
		parameters.addAttribute("id", this.projectId);
		parameters.addAttribute("tab", "finance");
		
		if (itemDetailCost.getNom_initial() instanceof Double == false){
			bindingResult.rejectValue("nom_initial","error.Cost","Dieser Wert muss eine Zahl sein");
			logger.trace("Der folgende Wert ist nicht vom Datentyp Double " + itemDetailCost.getNom_initial());
		}
		if (itemDetailCost.getNom_conception() instanceof Double == false){
			bindingResult.rejectValue("nom_conception","error.Cost","Dieser Wert muss eine Zahl sein");
			logger.trace("Der folgende Wert ist nicht vom Datentyp Double " + itemDetailCost.getNom_conception());
		}
		if (itemDetailCost.getNom_realization() instanceof Double == false){
			bindingResult.rejectValue("nom_realization","error.Cost","Dieser Wert muss eine Zahl sein");
			logger.trace("Der folgende Wert ist nicht vom Datentyp Double " + itemDetailCost.getNom_realization());
		}
		if (itemDetailCost.getNom_introduction() instanceof Double == false){
			bindingResult.rejectValue("nom_introduction","error.Cost","Dieser Wert muss eine Zahl sein");
			logger.trace("Der folgende Wert ist nicht vom Datentyp Double " + itemDetailCost.getNom_introduction());
		}
		if (itemDetailCost.getTgt_initial() instanceof Double == false){
			bindingResult.rejectValue("tgt_initial","error.Cost","Dieser Wert muss eine Zahl sein");
			logger.trace("Der folgende Wert ist nicht vom Datentyp Double " + itemDetailCost.getTgt_initial());
		}
		if (itemDetailCost.getTgt_conception() instanceof Double == false){
			bindingResult.rejectValue("tgt_conception","error.Cost","Dieser Wert muss eine Zahl sein");
			logger.trace("Der folgende Wert ist nicht vom Datentyp Double " + itemDetailCost.getTgt_conception());
		}
		if (itemDetailCost.getTgt_realization() instanceof Double == false){
			bindingResult.rejectValue("tgt_realization","error.Cost","Dieser Wert muss eine Zahl sein");
			logger.trace("Der folgende Wert ist nicht vom Datentyp Double " + itemDetailCost.getTgt_realization());
		}
		if (itemDetailCost.getTgt_introduction() instanceof Double == false){
			bindingResult.rejectValue("tgt_introduction","error.Cost","Dieser Wert muss eine Zahl sein");
			logger.trace("Der folgende Wert ist nicht vom Datentyp Double " + itemDetailCost.getTgt_introduction());
		}
		
		if (bindingResult.hasErrors()) {
			logger.trace("Parameter " + parameters);
			model.addAttribute("postAction", "/item/updateDetailCost");
			Project pr = itemRepo.getById(this.projectId);
			State progress = stateRepo.getLatestProgressById(projectId);
			model.addAttribute("itemDetailProgress", progress);
			model.addAttribute("itemDetail", pr);
			model.addAttribute("tabField", "finance");
			//setCostControls(model);
			return "itemDetailTemplate";
		}
		
		logger.trace("POST /item/updateCost mit itemDetailCost " + itemDetailCost);
		
		costRepo.save(itemDetailCost);
		
		return "redirect:/item";
	}
	

	/**
	 * RequestMapping /item/updateTime: Erstellt ein neues Time Item in der DB. Die id wird von der Datenbank erstellt.
	 * Bei Erfolg wird wieder auf die itemseite redirected (z.B.: /item&id=99  wenn die id 99 war.)
	 * 
	 * Hört auf POST /item/updateTime
	 * 
	 * @param model
	 * @param parameters
	 * @param itemDetailTime
	 * @param bindingResult
	 * @return Redirect auf /item
	 */
	@RequestMapping(method=POST, path="/item/updateTime")
	String newTime(Model model, RedirectAttributes parameters, @ModelAttribute("itemDetailTime") @Valid Time itemDetailTime, BindingResult bindingResult)
	{
		parameters.addAttribute("id", this.projectId);
		parameters.addAttribute("tab", "time");
		
		if (bindingResult.hasErrors()) {
			logger.trace("Parameter " + parameters);
			model.addAttribute("postAction", "/item/updateDetailTime");
			Project pr = itemRepo.getById(this.projectId);
			model.addAttribute("itemDetail", pr);
			model.addAttribute("tabField", "time");

			return "itemDetailTemplate";
		}
		
		logger.trace("POST /item/updateTime mit itemDetailTime " + itemDetailTime);
		timeRepo.save(itemDetailTime);
		
		return "redirect:/item";
	}
	
	
	/**
	 * RequestMapping /item/newState: Erstellt ein neues State Item in der Datenbank. Die id wird von der Datenbank erstellt.
	 * Bei Erfolg wird wieder auf die itemseite redirected (z.B.: /item&id=99  wenn die id 99 war.)
	 * 
	 * Hört auf POST /item/updateState
	 * 
	 * @param model
	 * @param parameters
	 * @param itemDetailState
	 * @param bindingResult
	 * @return Redirect auf /item
	 */
	@RequestMapping(method=POST, path="/item/newState")
	String newState(Model model, RedirectAttributes parameters, @ModelAttribute("itemDetailState") @Valid State itemDetailState, BindingResult bindingResult)
	{	
		parameters.addAttribute("id", this.projectId);
		parameters.addAttribute("tab", "status");
		
		if (bindingResult.hasErrors()) {
			
			if(bindingResult.hasFieldErrors("comment")) {
				parameters.addFlashAttribute("error_comment", "Maximal 255 Zeichen sind erlaubt");
			}
			parameters.addFlashAttribute("itemDetailState", itemDetailState);
			return "redirect:/item";
		}

		logger.trace("POST /item/newState mit itemDetailState " + itemDetailState);
		stateRepo.insert(itemDetailState);
		
		return "redirect:/item";
	}
	

	/**
	 * RequestMapping /item/newRisk: Erstellt ein neues Risk Item in der Datenbank. Die id wird von der Datenbank erstellt.
	 * Bei Erfolg wird wieder auf die itemseite redirected (z.B.: /item&id=99  wenn die id 99 war.)
	 * 
	 * Hört auf POST /item/newRisk
	 * 
	 * @param model
	 * @param parameters
	 * @param itemDetailRisk
	 * @param bindingResult
	 * @return Redirect auf /item
	 */
	@RequestMapping(method=POST, path="/item/newRisk")
	String newRisk(Model model, RedirectAttributes parameters, @ModelAttribute("itemDetailRisk") @Valid Risk itemDetailRisk, BindingResult bindingResult)
	{
		parameters.addAttribute("id", this.projectId);
		parameters.addAttribute("tab", "risk");
		
		if (bindingResult.hasErrors()) {
			
			if(bindingResult.hasFieldErrors("description")) {
				parameters.addFlashAttribute("error_description", "Maximal 255 Zeichen sind erlaubt");
			}
			parameters.addFlashAttribute("itemDetailRisk", itemDetailRisk);
			return "redirect:/item";
		}
		
		// die neue Id wird dem Redirect als Parameter hinzugefügt der redirect erfolgt dann auf /item&id=x
		if(this.projectId != 0){
			parameters.addAttribute("id", this.projectId);
		}
		
		logger.trace("POST /item/newRisk mit itemDetailRisk " + itemDetailRisk);
		
		riskRepo.insert(itemDetailRisk);
		
		return "redirect:/item";
	}

	
	/**
	 * RequestMapping /people: Anzeige aller erfassten Personen.
	 * @param model
	 * @return peopleDetailTemplate
	 */
	@RequestMapping(method=GET, value="/people")
	String getPeopleList(Model model)
	{
		logger.trace("/people");
		
		// Items werden geladen und die Collection dann für das Template unter dem namen "list" bereitgestellt
		// Das Template muss dann auch den Namen "list" verwenden.
		model.addAttribute("list", peopleRepo.getAll());
		model.addAttribute("postActionPeopleManagement", "/people/newPeople");
		
		return "peopleDetailTemplate";
	}
	
	
	/**
	 * RequestMapping /people/newPeople: Erstellt eine neue Person in der Ansicht (Seite /people) User Management
	 * @param model
	 * @param parameters
	 * @param peopleDetail
	 * @param bindingResult
	 * @return Redirect zurück zur Liste
	 */
	@RequestMapping(method=POST, path="/people/newPeople")
	String newPeopleManagement(Model model, RedirectAttributes parameters, @Valid People peopleDetail, BindingResult bindingResult)
	{
		logger.trace("POST /people/newPeople mit peopleDetail " + peopleDetail);
		
		if (bindingResult.hasErrors()) {
			logger.trace("bindingResult has errors " + bindingResult);
			return "peopleDetailTemplate";
		}
		
		// Insert der neuen Person durchführen
		peopleRepo.insert(peopleDetail);
		
		return "redirect:/people";
	}
	
	
	/**
	 * RequestMapping /people/delete: Löscht die Person mit der übergebenen id in der Datenbank
	 * 
	 * Hört auf GET /people/delete (besser wäre POST)
	 * 
	 * @param parameters (Java --> Redirect) Hier nicht verwendet da die Liste ohne Parameter aufgerufen wird
	 * @param id (WWW --> Java) Id des Items welches gelöscht werden soll.
	 * @return Redirect zurück zur Liste
	 */
	@RequestMapping(path="/people/delete")
	String deletePeople(RedirectAttributes parameters, @RequestParam Integer id, @Valid People peopleDetail, BindingResult bindingResult)
	{
		logger.trace("GET /people/delete mit id " + id);
		
		peopleRepo.delete(id);
		
		if (bindingResult.hasErrors()) {
			logger.trace("GET /people/delete has errors " + bindingResult);
			parameters.addFlashAttribute("userDeleteError", "User kann nicht gelöscht werden und wird verwendet");
			parameters.addFlashAttribute("peopleDetail", peopleDetail);
			bindingResult.reject("userDeleteError", "User kann nicht gelöscht werden und wird verwendet");
			
			return "redirect:/people";
		}
		
		return "redirect:/people";
	}
	
	
	/**
	 * Ausgelagerte Funktion welche Attribute für Personenfelder hinzufügt
	 * @param model
	 */
	private void setPeopleControls(Model model){
		model.addAttribute("list", peopleRepo.getAll());
		model.addAttribute("postActionPeople", "/item/newPeople");
	}

}