package ch.bfh.milklist.domain;

import java.sql.Date;

import javax.validation.constraints.Size;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Future;

/**
 * Einzelner Eintrag in der Liste mit Name und Menge (und einer eindeutigen Id)
 * @author Marcel Briggen
 */
public class Project {
	private int id;
	@Size (min = 2, max = 25,
			message = "Der Projektname muss zwischen 2 und 25 Zeichen lang sein"
		  )
	@Pattern (
			regexp = "^[\\p{L}0-9a-zA-Z ]*$",
			message = "Der Projektname darf nur folgende Zeichen beinhalten: A-Z, 0-9 und Leerzeichen"
			)
	private String name;
	private Date start;
	@Future(message = "Das Enddatum muss in der Zukunft sein")
	private Date end;
	private String project_phase;
	private int customer_id;
	private int projectleader_id;
	private String projectleader;
	private String customer;
	
	/**
	 * Defaultkonstruktor für die Verwendung in einem Controller
	 */
	public Project()
	{
		
	}
	
	/**
	 * Konstruktor
	 * @param id
	 * @param name
	 * @param start
	 * @param end
	 * @param project_phase
	 * @param customer_id
	 * @param projectleader_id
	 * @param customer
	 * @param projectleader
	 */
	public Project(int id, String name, Date start, Date end, String project_phase, int customer_id, int projectleader_id,
			String customer, String projectleader) {
		this.id = id;
		this.name = name;
		this.start = start;
		this.end = end;
		this.project_phase = project_phase;
		this.customer_id = customer_id;
		this.projectleader_id = projectleader_id;
		this.customer = customer;
		this.projectleader = projectleader;
	}

	/**
	 * Setter und Getter Methoden
	 */
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getStart() {
		return start;
	}

	public void setStart(Date start) {
		this.start = start;
	}

	public Date getEnd() {
		return end;
	}

	public void setEnd(Date end) {
		this.end = end;
	}

	public String getProject_phase() {
		return project_phase;
	}

	public void setProject_phase(String project_phase) {
		this.project_phase = project_phase;
	}

	public int getCustomer_id() {
		return customer_id;
	}

	public void setCustomer_id(int customer_id) {
		this.customer_id = customer_id;
	}

	public int getProjectleader_id() {
		return projectleader_id;
	}

	public void setProjectleader_id(int projectleader_id) {
		this.projectleader_id = projectleader_id;
	}

	public String getCustomer() {
		return customer;
	}

	public void setCustomer(String customer) {
		this.customer = customer;
	}

	public String getProjectleader() {
		return projectleader;
	}

	public void setProjectleader(String projectleader) {
		this.projectleader = projectleader;
	}

	/**
	 * toString Methode für die Ausgabe des ganzen Objektes mit allen Variablen
	 */
	@Override
	public String toString() {
		return "Project [id=" + id + ", name=" + name + ", start=" + start + ", end="
				+ end + ", project_phase=" + project_phase + ", customer=" + customer + ", projectleader=" + projectleader + ", customer_id="
				+ customer_id + ", projectleader_id=" + projectleader_id + "]";
	}

}