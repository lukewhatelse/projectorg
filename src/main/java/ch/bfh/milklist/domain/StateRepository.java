package ch.bfh.milklist.domain;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

/**
 * Repository für alle Items. (Mit dem Spring JDBC-Template implementiert.)
 * Hier werden alle Operstionen für die DB-Operationen zu Items implementiert
 * @author Marcel Briggen
 *
 */
@Component() 
public class StateRepository {
	
	private final Logger logger = LoggerFactory.getLogger(StateRepository.class);
	
    @Autowired
	private JdbcTemplate jdbc;
	
	/**
	 * SELECT: Liefert den Status für das jeweilige Projekt mit der übergebenen Id.
	 * @param id id des Item
	 * @return Item oder NULL
	 */
	public Collection<State> getListById(int project_id) {
		logger.trace("getListById " + project_id);
		return jdbc.query("SELECT * FROM state WHERE project_id=? ORDER BY id DESC",new Object [] {project_id}, new StateMapper());
	}
	
	
	/**
	 * SELECT: Liefert den Status für das jeweilige Projekt mit der übergebenen Id.
	 * Wird kein Projekt mit dieser Id gefunden, wird ein leeres Object mit dieser project_id erstellt.
	 * @param project_id
	 * @return
	 */
	public State getById(int project_id) {
		logger.trace("getById " + project_id);
		
		State s = new State();
		
		// Wenn kein State Objekt vorhanden ist, wir ein leeres mit der projekt id angelegt
		if(jdbc.query("SELECT * FROM state WHERE project_id=? ORDER BY id DESC LIMIT 1", new Object [] {project_id}, new StateMapper()).isEmpty()) {
			s = new State(project_id);
		}else {
			s = jdbc.queryForObject("SELECT * FROM state WHERE project_id=? ORDER BY id DESC LIMIT 1",new Object [] {project_id}, new StateMapper());
		}

		return s;
	}
	
	
	/**
	 * SELECT: Liefert Projekt Status für das jeweilige Projekt.
	 * @param project_id
	 * @return
	 */
	public State getLatestProgressById(int project_id) {
		logger.trace("getLatestProgressById " + project_id);
		
		State s;

		// Wenn kein State Objekt vorhanden ist, wir ein leeres mit der projekt id und einem Status von 0 angelegt
		if(jdbc.query("SELECT project_id, progress FROM state WHERE project_id=? ORDER BY id DESC LIMIT 1", new Object [] {project_id}, new StateProgressMapper()).isEmpty()) {
			s = new State(project_id, (int)0);
		}else {
			s = jdbc.queryForObject("SELECT project_id, progress FROM state WHERE project_id=? ORDER BY id DESC LIMIT 1", new Object [] {project_id}, new StateProgressMapper());
		}
		return s;
	}

	
	/**
	 * INSERT: Speichert das angegebene Item in der Datenbank.
	 * @param i neu zu erstellendes Item
	 * @return Liefert die von der DB generierte id des neuen Items zurück
	 */
	public Integer insert(State s) {	
		logger.trace("insert " + s);
		jdbc.update("INSERT INTO state (project_id, period_start, period_end, cost, date, ressource, risk, progress, comment)"
		+ " VALUES (?,?,?,?,?,?,?,?,?)",s.getProject_id(),s.getPeriod_start(),s.getPeriod_end(),s.getCost(),s.getDate(),
		s.getRessource(),s.getRisk(),s.getProgress(),s.getComment());
		Integer id = jdbc.queryForObject("select IDENTITY();", Integer.class);

		return id;
	}

	
	/**
	 * Hilfsklasse zum Konvertieren der Resultsets in Item-Objekte. Siehe getAll & getById Methoden.
	 * @author Marcel Brigen
	 */
	private class StateMapper implements RowMapper<State>
	{
		@Override
		public State mapRow(ResultSet rs, int rownum) throws SQLException {
			State s = new State(rs.getInt("id"),rs.getInt("project_id"),rs.getDate("period_start"),rs.getDate("period_end"),rs.getInt("cost"),
					rs.getInt("date"),rs.getInt("ressource"),rs.getInt("risk"),rs.getInt("progress"),rs.getString("comment"));
			return s;
		}
	}
	
	
	/**
	 * Hilfsklasse zum Konvertieren der Resultsets in Item-Objekte. Siehe getLatestProgressById Methoden.
	 */
	private class StateProgressMapper implements RowMapper<State>
	{
		@Override
		public State mapRow(ResultSet rs, int rownum) throws SQLException {
			State s = new State(rs.getInt("project_id"),rs.getInt("progress"));
			return s;
		}
	}

}