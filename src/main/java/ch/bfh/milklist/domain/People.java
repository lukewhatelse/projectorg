package ch.bfh.milklist.domain;

import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

/**
 * Einzelner Eintrag in der Liste der Personen
 * @author Lucas Badertscher
 */
public class People {
	private int id;
	@Size(min=2, max=25)
	@Pattern(regexp = "^[\\p{L}0-9a-zA-ZäöüÄÖÜ]*$")
	private String firstname;
	@Size(min=2, max=25)
	@Pattern(regexp = "^[\\p{L}0-9a-zA-ZäöüÄÖÜ]*$")
	private String lastname;

	/**
	 * Defaultkonstruktor für die Verwendung in einem Controller
	 */
	public People()
	{
		
	}
	
	/**
	 * Konstruktor
	 * @param id Eindeutige Id
	 * @param firstname Vorname der Person
	 * @param lastname Nachname der Person
	 */
	public People(int id, String firstname, String lastname)
	{
		this.id = id;
		this.firstname = firstname;
		this.lastname = lastname;
	}

	/**
	 * Setter und Getter Methoden
	 */
	
	public int getId() {
		return id;
	}
	
	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}
	
	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	
	/**
	 * Setzt den ganzen Namen zusammen
	 * @return firstname + lastname
	 */
	public String getFullname(){
		return this.firstname + " " + this.lastname;
	}
	
	/**
	 * toString Methode für die Ausgabe des ganzen Objektes mit allen Variablen
	 */
	@Override
	public String toString() {
		return "People [id=" + id + ", firstname=" + firstname + ", lastname=" + lastname + "]";
	}
	
}