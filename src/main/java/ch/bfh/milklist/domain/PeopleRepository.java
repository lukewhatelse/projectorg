package ch.bfh.milklist.domain;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

/**
 * Repository für alle Items. (Mit dem Spring JDBC-Template implementiert.)
 * Hier werden alle Operstionen für die DB-Operationen zu Items implementiert
 * @author Marcel Briggen
 */
@Component() 
public class PeopleRepository {
	
	private final Logger logger = LoggerFactory.getLogger(PeopleRepository.class);
	
    @Autowired
	private JdbcTemplate jdbc;

	/**
	 * SELECT: Liefert alle items in der Datenbank
	 * @return Collection aller Items
	 */
	public Collection<People> getAll()  {
		logger.trace("getAll");
		return jdbc.query("SELECT * FROM people", new PeopleMapper());
	}

	
	/**
	 * SELECT: Liefert das Item mit der übergebenen Id
	 * @param id des Items
	 * @return Item oder NULL
	 */
	public People getById(int id) {
		logger.trace("getById " + id);
		return jdbc.queryForObject("SELECT * FROM people WHERE id=?", new Object [] {id}, new PeopleMapper());
	}
	
	
	/**
	 * INSERT: Speichert das Item in die Datenbank
	 * @param p
	 * @return id der erstellten Person
	 */
	public Integer insert(People p) {
		logger.trace("insert " + p);
		jdbc.update("INSERT INTO people(firstname, lastname) VALUES (?,?)", p.getFirstname(), p.getLastname());
		Integer id = jdbc.queryForObject("select IDENTITY();", Integer.class);
		
		return id;
	}
	
	
	/**
	 * DELETE: Löscht das Item mit der angegebenen Id von der DB
	 * @param id People ID
	 */
	public boolean delete(Integer id) {
		boolean check;
		
		if(jdbc.query("SELECT * FROM people WHERE id=? AND (SELECT COUNT(*) FROM project AS p INNER JOIN"
				+ " risk AS r WHERE customer_id=? OR projectleader_id=? OR responsible_id=?)>0",new Object[] {id, id, id, id}, new PeopleMapper()).isEmpty()) {
			logger.trace("delete people with id=" + id);
			jdbc.update("DELETE FROM people WHERE id=?",id);
			check = true;
		} else {
			logger.trace("people with id=" + id + " has references and can not be delete");
			check = false;
		}
		
		return check;
	}
	
	
	/**
	 * Hilfsklasse zum Konvertieren der Resultsets in Item-Objekte. Siehe getAll & getById Methoden.
	 * @author Marcel Brigen
	 */
	private class PeopleMapper implements RowMapper<People>
	{
		@Override
		public People mapRow(ResultSet rs, int rownum) throws SQLException {
			People p = new People(rs.getInt("id"),rs.getString("firstname"),rs.getString("lastname"));
			return p;
		}
	}

}