package ch.bfh.milklist.domain;

import java.sql.Date;

import javax.validation.constraints.Size;

/**
 * Einzelner Eintrag in der Liste mit Name und Menge (und einer eindeutigen Id)
 * @author Marcel Briggen
 *
 */
public class State {
	private int id;
	private int project_id;
	private Date period_start;
	private Date period_end;
	private int cost;
	private String cost_text;
	private int date;
	private String date_text;
	private int ressource;
	private String ressource_text;
	private int risk;
	private String risk_text;
	private int progress;
	private String progress_text;
	private String overall_text;
	@Size(max=255)
	private String comment;
	
	/**
	 * Defaultkonstruktor für die Verwendung in einem Controller
	 */
	public State()
	{
		
	}
	
	/**
	 * Konstruktor für leeres Objekt, an welchem die projekt ID übergeben wird.
	 * @param project_id
	 */
	public State(int project_id) {
		this.project_id = project_id;
	}
	
	/**
	 * Konstruktor für leeres Objekt, an welchem die Projekt ID und Status übergeben wird.
	 * @param project_id
	 * @param progress
	 */
	public State (int project_id, int progress) {
		this.project_id = project_id;
		this.progress = progress;
		this.progress_text = progress + "%";
	}
	
	/**
	 * Konstruktor ohne id aus der Datenbank
	 * @param project_id
	 * @param period_start
	 * @param period_end
	 * @param cost
	 * @param date
	 * @param ressource
	 * @param risk
	 * @param progress
	 * @param comment
	 */
	public State(int project_id, Date period_start, Date period_end, int cost, int date, 
			int ressource, int risk, int progress, String comment) {
		this.project_id = project_id;
		this.period_start = period_start;
		this.period_end = period_end;
		this.cost = cost;
		this.date = date;
		this.ressource = ressource;
		this.risk = risk;
		this.progress = progress;
		this.comment = comment;
		
		this.cost_text = transformToText(cost);
		this.date_text = transformToText(date);
		this.ressource_text = transformToText(ressource);
		this.risk_text = transformToText(risk);
		this.progress_text = progress + "%";
	}
	
	/**
	 * Konstruktor mit id aus der Datenbank
	 * @param id
	 * @param project_id
	 * @param period_start
	 * @param period_end
	 * @param cost
	 * @param date
	 * @param ressource
	 * @param risk
	 * @param progress
	 * @param comment
	 */
	public State(int id, int project_id, Date period_start, Date period_end, int cost, int date, 
			int ressource, int risk, int progress, String comment) {
		this.id = id;
		this.project_id = project_id;
		this.period_start = period_start;
		this.period_end = period_end;
		this.cost = cost;
		this.date = date;
		this.ressource = ressource;
		this.risk = risk;
		this.progress = progress;
		this.comment = comment;
		
		this.cost_text = transformToText(cost);
		this.date_text = transformToText(date);
		this.ressource_text = transformToText(ressource);
		this.risk_text = transformToText(risk);
		this.progress_text = progress + "%";
	}
	
	/**
	 * Konstruktor für NULL Werte
	 * @param project_id
	 * @param cost
	 * @param date
	 * @param ressource
	 * @param risk
	 * @param progress
	 * @param comment
	 */
	public State(int project_id, int cost, int date, 
			int ressource, int risk, int progress, String comment) {
		this.project_id = project_id;
		this.cost = cost;
		this.date = date;
		this.ressource = ressource;
		this.risk = risk;
		this.progress = progress;
		this.comment = comment;
	}
	
	/**
	 * Setter und Getter Methoden
	 */
	
	public int getId() {
		return id;
	}

	public int getProject_id() {
		return project_id;
	}

	public void setProject_id(int project_id) {
		this.project_id = project_id;
	}

	public Date getPeriod_start() {
		return period_start;
	}

	public void setPeriod_start(Date period_start) {
		this.period_start = period_start;
	}

	public Date getPeriod_end() {
		return period_end;
	}

	public void setPeriod_end(Date period_end) {
		this.period_end = period_end;
	}

	public int getCost() {
		return cost;
	}

	public void setCost(int cost) {
		this.cost = cost;
	}

	public String getCost_text() {
		return cost_text;
	}

	public void setCost_text(String cost_text) {
		this.cost_text = cost_text;
	}

	public int getDate() {
		return date;
	}

	public void setDate(int date) {
		this.date = date;
	}

	public String getDate_text() {
		return date_text;
	}

	public void setDate_text(String date_text) {
		this.date_text = date_text;
	}

	public int getRessource() {
		return ressource;
	}

	public void setRessource(int ressource) {
		this.ressource = ressource;
	}

	public String getRessource_text() {
		return ressource_text;
	}

	public void setRessource_text(String ressource_text) {
		this.ressource_text = ressource_text;
	}

	public int getRisk() {
		return risk;
	}

	public void setRisk(int risk) {
		this.risk = risk;
	}

	public String getRisk_text() {
		return risk_text;
	}

	public void setRisk_text(String risk_text) {
		this.risk_text = risk_text;
	}

	public int getProgress() {
		return progress;
	}

	public void setProgress(int progress) {
		this.progress = progress;
	}

	public String getProgress_text() {
		return progress_text;
	}

	public String getOverall_text() {
		return overall_text;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}
	
	/**
	 * Methode welche für den Projekt Status von Nummern in Text umwandelt
	 * @param number
	 */
	public String transformToText(int number) {
		String textToReturn;
		if (number == 1) {
			textToReturn = "gruen";
		} else if (number == 2) {
			textToReturn = "gelb";
		} else if (number == 3) {
			textToReturn = "rot";
		} else {
			textToReturn = null;
		}
		
		return textToReturn;
	}

	/**
	 * toString Methode für die Ausgabe des ganzen Objektes mit allen Variablen
	 */
	@Override
	public String toString() {
		return "State [id=" + id + ", project_id=" + project_id + ", period_start=" + period_start + ", period_end=" + period_end
				+ ", cost=" + cost + ", date=" + date + ", ressource=" + ressource + ", risk=" + risk + ", progress=" + progress
				+ ", comment=" + comment + "]";
	}
	
}