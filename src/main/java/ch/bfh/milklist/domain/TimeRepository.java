package ch.bfh.milklist.domain;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

/**
 * Repository für alle Items. (Mit dem Spring JDBC-Template implementiert.)
 * Hier werden alle Operstionen für die DB-Operationen zu Items implementiert
 * @author Lucas Badertscher 
 */
@Component()
public class TimeRepository {
	
	private final Logger logger = LoggerFactory.getLogger(TimeRepository.class);
	
    @Autowired
	private JdbcTemplate jdbc;

	
	/**
	 * SELECT: Liefert die Zeiten für das jeweilige Projekt mit der übergebenen Id.
	 * Wird kein Projekt mit dieser Id gefunden, wird ein leerer Datensatz der Zeiten für dieses Projekt erstellt
	 * @param id des Projektes
	 * @return Item oder NULL
	 */
	public Time getById(int project_id) {
		logger.trace("getById " + project_id);
		
		Time t = new Time();
		
		// Wenn kein Time Objekt vorhanden ist, wir ein leeres angelegt und in die DB gefüllt
		if(jdbc.query("SELECT * FROM time WHERE project_id=?",new Object [] {project_id}, new TimeMapper()).isEmpty()){
			t = jdbc.queryForObject("SELECT id, start, end FROM project WHERE id=?",new Object [] {project_id}, new TimeStartEndMapper());
			this.insertNullValue(t);
		}else {
			t = jdbc.queryForObject("SELECT * FROM time WHERE project_id=?",new Object [] {project_id}, new TimeMapper());
		}
		
		logger.trace("getById: " + t);
		
		return t;
	}
	
	
	/**
	 * INSERT: Speichert das angegebene Item mit NULL-Werte in der Datenbank.
	 * @param t neu zu erstellendes Item
	 * @return Liefert die von der DB generierte id des neuen Items zurück
	 */
	public Integer insertNullValue(Time t) {
		logger.trace("insertNullValue " + t);
		jdbc.update("INSERT INTO time (project_id, start_ist, start_soll, end_ist, end_soll, nom_initial_ist, nom_initial_soll, nom_conception_ist,"
				+ " nom_conception_soll, nom_realization_ist, nom_realization_soll, nom_introduction_ist, nom_introduction_soll, tgt_initial_ist,"
				+ " tgt_initial_soll, tgt_conception_ist, tgt_conception_soll, tgt_realization_ist, tgt_realization_soll, tgt_introduction_ist,"
				+ " tgt_introduction_soll) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)",
				t.getProject_id(),t.getStart_ist(),t.getStart_soll(),t.getEnd_ist(),t.getEnd_soll(),
				t.getNom_initial_ist(),t.getNom_initial_soll(),t.getNom_conception_ist(),t.getNom_conception_soll(),t.getNom_realization_ist(),
				t.getNom_realization_soll(),t.getNom_introduction_ist(),t.getNom_introduction_soll(),t.getTgt_initial_ist(),t.getTgt_initial_soll(),
				t.getTgt_conception_ist(),t.getTgt_conception_soll(),t.getTgt_realization_ist(),t.getTgt_realization_soll(),t.getTgt_introduction_ist(),
				t.getTgt_introduction_soll());
		Integer id = jdbc.queryForObject("select IDENTITY();", Integer.class);

		return id;
	}
	
	
	/**
	 * INSERT: Speichert das angegebene Item in der Datenbank.
	 * @param t neu zu erstellendes Item
	 * @return Liefert die von der DB generierte id des neuen Items zurück
	 */
	public Integer insert(Time t) {
		logger.trace("insert " + t);
		jdbc.update("INSERT INTO time (project_id, start_ist, start_soll, end_ist, end_soll, nom_initial_ist, nom_initial_soll, nom_conception_ist,"
				+ " nom_conception_soll, nom_realization_ist, nom_realization_soll, nom_introduction_ist, nom_introduction_soll, tgt_initial_ist,"
				+ " tgt_initial_soll, tgt_conception_ist, tgt_conception_soll, tgt_realization_ist, tgt_realization_soll, tgt_introduction_ist,"
				+ " tgt_introduction_soll) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)",t.getProject_id(),t.getStart_ist(),t.getStart_soll(),
				t.getEnd_ist(),t.getEnd_soll(),t.getNom_initial_ist(),t.getNom_initial_soll(),t.getNom_conception_ist(),t.getNom_conception_soll(),
				t.getNom_realization_ist(),t.getNom_realization_soll(),t.getNom_introduction_ist(),t.getNom_introduction_soll(),t.getTgt_initial_ist(),
				t.getTgt_initial_soll(),t.getTgt_conception_ist(),t.getTgt_conception_soll(),t.getTgt_realization_ist(),t.getTgt_realization_soll(),
				t.getTgt_introduction_ist(),t.getTgt_introduction_soll());
		Integer id = jdbc.queryForObject("select IDENTITY();", Integer.class);

		return id;
	}
	
	
	/**
	 * UPDATE: Speichert das übergebene item in der Datenbank.
	 * @param t
	 */
	public void save(Time t) {
		logger.trace("save " + t);
		jdbc.update("UPDATE time SET start_ist=?, start_soll=?, end_ist=?, end_soll=?, nom_initial_ist=?, nom_initial_soll=?, nom_conception_ist=?,"
				+ " nom_conception_soll=?, nom_realization_ist=?, nom_realization_soll=?, nom_introduction_ist=?, nom_introduction_soll=?,"
				+ " tgt_initial_ist=?, tgt_initial_soll=?, tgt_conception_ist=?, tgt_conception_soll=?, tgt_realization_ist=?, tgt_realization_soll=?,"
				+ " tgt_introduction_ist=?, tgt_introduction_soll=? WHERE project_id=?",t.getStart_ist(),t.getStart_soll(),t.getEnd_ist(),t.getEnd_soll(),
				t.getNom_initial_ist(),t.getNom_initial_soll(),t.getNom_conception_ist(),t.getNom_conception_soll(),t.getNom_realization_ist(),
				t.getNom_realization_soll(),t.getNom_introduction_ist(),t.getNom_introduction_soll(),t.getTgt_initial_ist(),t.getTgt_initial_soll(),
				t.getTgt_conception_ist(),t.getTgt_conception_soll(),t.getTgt_realization_ist(),t.getTgt_realization_soll(),t.getTgt_introduction_ist(),
				t.getTgt_introduction_soll(),t.getProject_id());
	}
	
	
	/**
	 * Hilfsklasse zum Konvertieren der Resultsets in Item-Objekte. Siehe getAll & getById Methoden.
	 * @author Lucas Badertscher
	 *
	 */
	private class TimeMapper implements RowMapper<Time>
	{
		@Override
		public Time mapRow(ResultSet rs, int rownum) throws SQLException {
			Time t = new Time(rs.getInt("id"),rs.getInt("project_id"),rs.getDate("start_ist"),rs.getDate("start_soll"),rs.getDate("end_ist"),rs.getDate("end_soll"),
					rs.getDate("nom_initial_ist"),rs.getDate("nom_initial_soll"),rs.getDate("nom_conception_ist"),
					rs.getDate("nom_conception_soll"),rs.getDate("nom_realization_ist"),rs.getDate("nom_realization_soll"),
					rs.getDate("nom_introduction_ist"),rs.getDate("nom_introduction_soll"),rs.getDate("tgt_initial_ist"),
					rs.getDate("tgt_initial_soll"),rs.getDate("tgt_conception_ist"),rs.getDate("tgt_conception_soll"),
					rs.getDate("tgt_realization_ist"),rs.getDate("tgt_realization_soll"),rs.getDate("tgt_introduction_ist"),
					rs.getDate("tgt_introduction_soll"));
			return t;
		}
	}
	
	
	/**
	 * Hilfsklasse zum Konvertieren der Resultsets in Item-Objekte. Siehe insertNullValue Methoden.
	 * @author Fabian Fischer
	 *
	 */	
	private class TimeStartEndMapper implements RowMapper<Time>
	{
		@Override
		public Time mapRow(ResultSet rs, int rownum) throws SQLException {
			Time t = new Time(rs.getInt("id"),rs.getDate("start"),rs.getDate("end"));
			return t;
		}
	}
	
}