package ch.bfh.milklist.domain;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;


/**
 * Repository für alle Items. (Mit dem Spring JDBC-Template implementiert.)
 * Hier werden alle Operstionen für die DB-Operationen zu Items implementiert
 * @author Marcel Briggen
 *
 */
@Component() 
public class RiskRepository {
	
	private final Logger logger = LoggerFactory.getLogger(RiskRepository.class);
	
    @Autowired
	private JdbcTemplate jdbc;
	
	
	/**
	 * SELECT: Liefert das Item mit der übergebenen Id
	 * @param id id des Item
	 * @return Item oder NULL
	 */
	public Collection<Risk> getListById(int project_id) {
		logger.trace("getListById " + project_id);
		return jdbc.query("SELECT r.id, r.project_id, r.description, r.measures, r.etw, r.impact, r.critical, c.firstname AS responsible_firstname,"
				+ " c.lastname AS responsible_lastname, r.responsible_id, r.phase, r.last_edited"
				+ " FROM risk AS r"
				+ " INNER JOIN people AS c ON r.responsible_id = c.id"
				+ " WHERE project_id=? ORDER BY id DESC",new Object [] {project_id}, new RiskMapperResponsible());
	}
	
	
	/**
	 * SELECT: Liefert die Kosten für das jeweilige Projekt mit der übergebenen Id.
	 * Wird kein Projekt mit dieser Id gefunden, wird ein leeres Object mit dieser project_id erstellt.
	 * @param project_id
	 * @return
	 */
	public Risk getById(int project_id) {
		logger.trace("getById " + project_id);
		
		Risk r = new Risk();
		
		//Wenn kein Risk Objekt vorhanden ist, wir ein leeres angelegt
		if(jdbc.query("SELECT * FROM risk WHERE project_id=? ORDER BY id DESC LIMIT 1", new Object [] {project_id}, new RiskMapper()).isEmpty()) {
			r = new Risk(project_id);
		}else {
			r = jdbc.queryForObject("SELECT * FROM risk WHERE project_id=? ORDER BY id DESC LIMIT 1",new Object [] {project_id}, new RiskMapper());
		}

		return r;
	}
	
	
	/**
	 * INSERT: Speichert das angegebene Item in der Datenbank.
	 * @param r neu zu erstellendes Item
	 * @return Liefert die von der DB generierte id des neuen Items zurück
	 */
	public Integer insert(Risk r) {
		logger.trace("insert " + r);
		jdbc.update("INSERT INTO risk (project_id, description, measures, etw, impact, critical, responsible_id, phase, last_edited)"
				+ " VALUES (?,?,?,?,?,?,?,?,?)",r.getProject_id(),r.getDecription(),r.getMeasures(),r.getEtw(),r.getImpact(),r.getCritical(),
				r.getResponsible_id(),r.getState_phase(),r.getLast_edited());
		Integer id = jdbc.queryForObject("select IDENTITY();", Integer.class);

		return id;
	}
	
	
	/**
	 * Hilfsklasse zum Konvertieren der Resultsets in Item-Objekte. Siehe getAll & getById Methoden.
	 * @author Lucas Badertscher
	 *
	 */
	private class RiskMapper implements RowMapper<Risk>
	{
		@Override
		public Risk mapRow(ResultSet rs, int rownum) throws SQLException {
			Risk r = new Risk(rs.getInt("id"),rs.getInt("project_id"),rs.getString("description"),rs.getString("measures"),rs.getFloat("etw"),rs.getInt("impact"),
					rs.getInt("critical"),rs.getInt("responsible_id"),rs.getString("phase"),rs.getDate("last_edited"));
			return r;
		}
	}
	
	
	/**
	 * Hilfsklasse zum Konvertieren des SQL Outputs in Item-Objekte.
	 * Wird bei der Methode getListById benötigt, da hier zusätzlich der Verantwortliche aus der people Tabelle gespeichert wird.
	 * @author Fabian
	 *
	 */
	private class RiskMapperResponsible implements RowMapper<Risk>
	{
		@Override
		public Risk mapRow(ResultSet rs, int rownum) throws SQLException {
			Risk r = new Risk(rs.getInt("id"),rs.getInt("project_id"),rs.getString("description"),rs.getString("measures"),rs.getFloat("etw"),rs.getInt("impact"),
					rs.getInt("critical"),rs.getString("responsible_firstname"),rs.getString("responsible_lastname"),rs.getInt("responsible_id"),
					rs.getString("phase"),rs.getDate("last_edited"));
			return r;
		}
	}
	
}