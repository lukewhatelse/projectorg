package ch.bfh.milklist.domain;

/**
 * Einzelner Eintrag in der Liste mit Name und Menge (und einer eindeutigen Id)
 * @author Marcel Briggen
 */
public class Cost {
	private int id;
	private int project_id;
	private Double nom_initial;
	private Double nom_conception;
	private Double nom_realization;
	private Double nom_introduction;
	private Double tgt_initial;
	private Double tgt_conception;
	private Double tgt_realization;
	private Double tgt_introduction;

	/**
	 * Default Konstruktor für die Verwendung in einem Controller
	 */
	public Cost()
	{
		
	}
	
	/**
	 * Konstruktor ohne id aus Datenbank
	 * @param project_id
	 * @param nom_initial
	 * @param nom_conception
	 * @param nom_realization
	 * @param nom_introduction
	 * @param tgt_initial
	 * @param tgt_conception
	 * @param tgt_realization
	 * @param tgt_introduction
	 */
	public Cost(int project_id, Double nom_initial, Double nom_conception, Double nom_realization, Double nom_introduction, 
			Double tgt_initial, Double tgt_conception, Double tgt_realization, Double tgt_introduction) {
		this.project_id = project_id;
		this.nom_initial = nom_initial;
		this.nom_conception = nom_conception;
		this.nom_realization = nom_realization;
		this.nom_introduction = nom_introduction;
		this.tgt_initial = tgt_initial;
		this.tgt_conception = tgt_conception;
		this.tgt_realization = tgt_realization;
		this.tgt_introduction = tgt_introduction;
	}
	
	/**
	 * Konstruktor mit id aus Datenbank
	 * @param id
	 * @param project_id
	 * @param nom_initial
	 * @param nom_conception
	 * @param nom_realization
	 * @param nom_introduction
	 * @param tgt_initial
	 * @param tgt_conception
	 * @param tgt_realization
	 * @param tgt_introduction
	 */
	public Cost(int id, int project_id, Double nom_initial, Double nom_conception, Double nom_realization, Double nom_introduction, 
			Double tgt_initial, Double tgt_conception, Double tgt_realization, Double tgt_introduction) {
		this.id = id;
		this.project_id = project_id;
		this.nom_initial = nom_initial;
		this.nom_conception = nom_conception;
		this.nom_realization = nom_realization;
		this.nom_introduction = nom_introduction;
		this.tgt_initial = tgt_initial;
		this.tgt_conception = tgt_conception;
		this.tgt_realization = tgt_realization;
		this.tgt_introduction = tgt_introduction;
	}

	/**
	 * Setter und Getter Methoden
	 */
	
	public int getId() {
		return id;
	}
	
	public int getProject_id() {
		return project_id;
	}

	public void setProject_id(int project_id) {
		this.project_id = project_id;
	}

	public Double getNom_initial() {
		return nom_initial;
	}

	public void setNom_initial(Double nom_initial) {
		this.nom_initial = nom_initial;
	}

	public Double getNom_conception() {
		return nom_conception;
	}

	public void setNom_conception(Double nom_conception) {
		this.nom_conception = nom_conception;
	}

	public Double getNom_realization() {
		return nom_realization;
	}

	public void setNom_realization(Double nom_realization) {
		this.nom_realization = nom_realization;
	}

	public Double getNom_introduction() {
		return nom_introduction;
	}

	public void setNom_introduction(Double nom_introduction) {
		this.nom_introduction = nom_introduction;
	}

	public Double getTgt_initial() {
		return tgt_initial;
	}

	public void setTgt_initial(Double tgt_initial) {
		this.tgt_initial = tgt_initial;
	}

	public Double getTgt_conception() {
		return tgt_conception;
	}

	public void setTgt_conception(Double tgt_conception) {
		this.tgt_conception = tgt_conception;
	}

	public Double getTgt_realization() {
		return tgt_realization;
	}

	public void setTgt_realization(Double tgt_realization) {
		this.tgt_realization = tgt_realization;
	}

	public Double getTgt_introduction() {
		return tgt_introduction;
	}

	public void setTgt_introduction(Double tgt_introduction) {
		this.tgt_introduction = tgt_introduction;
	}

	/**
	 * toString Methode für die Ausgabe des ganzen Objektes mit allen Variablen
	 */
	@Override
	public String toString() {		
		return "Cost [id=" + id + ", project_id=" + project_id + ", nom_initial=" + nom_initial + ", nom_conception="
				+ nom_conception + ", nom_realization=" + nom_realization + ", nom_introduction="
				+ nom_introduction + ", tgt_initial=" + tgt_initial + ", tgt_conception="
				+ tgt_conception + ", tgt_realization=" + tgt_realization + ", tgt_introduction=" + tgt_introduction + "]";
	}

}