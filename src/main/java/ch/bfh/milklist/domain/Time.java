package ch.bfh.milklist.domain;

import java.sql.Date;

import javax.validation.constraints.Future;

/**
 * Einzelner Eintrag in der Liste mit Name und Menge (und einer eindeutigen Id)
 * @author Lucas Badertscher
 *
 */
public class Time {
	private int id;
	private int project_id;
	private Date start_ist;
	private Date start_soll;
	@Future(message = "Das Enddatum muss in der Zukunft sein")
	private Date end_ist;
	@Future(message = "Das Enddatum muss in der Zukunft sein")
	private Date end_soll;
	private Date nom_initial_ist;
	private Date nom_initial_soll;
	private Date nom_conception_ist;
	private Date nom_conception_soll;
	private Date nom_realization_ist;
	private Date nom_realization_soll;
	private Date nom_introduction_ist;
	private Date nom_introduction_soll;
	private Date tgt_initial_ist;
	private Date tgt_initial_soll;
	private Date tgt_conception_ist;
	private Date tgt_conception_soll;
	private Date tgt_realization_ist;
	private Date tgt_realization_soll;
	private Date tgt_introduction_ist;
	private Date tgt_introduction_soll;
	
	/**
	 * Defaultkonstruktor für die Verwendung in einem Controller
	 */
	public Time()
	{
		
	}

	/**
	 * Konstruktor mit id aus der Datenbank
	 * @param id
	 * @param project_id
	 * @param start_ist
	 * @param start_soll
	 * @param end_ist
	 * @param end_soll
	 * @param nom_initial_ist
	 * @param nom_initial_soll
	 * @param nom_conception_ist
	 * @param nom_conception_soll
	 * @param nom_realization_ist
	 * @param nom_realization_soll
	 * @param nom_introduction_ist
	 * @param nom_introduction_soll
	 * @param tgt_initial_ist
	 * @param tgt_initial_soll
	 * @param tgt_conception_ist
	 * @param tgt_conception_soll
	 * @param tgt_realization_ist
	 * @param tgt_realization_soll
	 * @param tgt_introduction_ist
	 * @param tgt_introduction_soll
	 */
	public Time(int id, int project_id, Date start_ist, Date start_soll, Date end_ist, Date end_soll, Date nom_initial_ist, Date nom_initial_soll,
			Date nom_conception_ist, Date nom_conception_soll, Date nom_realization_ist, Date nom_realization_soll, Date nom_introduction_ist,
			Date nom_introduction_soll, Date tgt_initial_ist, Date tgt_initial_soll, Date tgt_conception_ist, Date tgt_conception_soll,
			Date tgt_realization_ist, Date tgt_realization_soll, Date tgt_introduction_ist, Date tgt_introduction_soll) {
		this.id = id;
		this.project_id = project_id;
		this.start_ist = start_ist;
		this.start_soll = start_soll;
		this.end_ist = end_ist;
		this.end_soll = end_soll;
		this.nom_initial_ist = nom_initial_ist;
		this.nom_initial_soll = nom_initial_soll;
		this.nom_conception_ist = nom_conception_ist;
		this.nom_conception_soll = nom_conception_soll;
		this.nom_realization_ist = nom_realization_ist;
		this.nom_realization_soll = nom_realization_soll;
		this.nom_introduction_ist = nom_introduction_ist;
		this.nom_introduction_soll = nom_introduction_soll;
		this.tgt_initial_ist = tgt_initial_ist;
		this.tgt_initial_soll = tgt_initial_soll;
		this.tgt_conception_ist = tgt_conception_ist;
		this.tgt_conception_soll = tgt_conception_soll;
		this.tgt_realization_ist = tgt_realization_ist;
		this.tgt_realization_soll = tgt_realization_soll;
		this.tgt_introduction_ist = tgt_introduction_ist;
		this.tgt_introduction_soll = tgt_introduction_soll;
	}
	
	/**
	 * Konstruktur ohne id aus der Datenbank
	 * @param project_id
	 * @param start_ist
	 * @param start_soll
	 * @param end_ist
	 * @param end_soll
	 * @param nom_initial_ist
	 * @param nom_initial_soll
	 * @param nom_conception_ist
	 * @param nom_conception_soll
	 * @param nom_realization_ist
	 * @param nom_realization_soll
	 * @param nom_introduction_ist
	 * @param nom_introduction_soll
	 * @param tgt_initial_ist
	 * @param tgt_initial_soll
	 * @param tgt_conception_ist
	 * @param tgt_conception_soll
	 * @param tgt_realization_ist
	 * @param tgt_realization_soll
	 * @param tgt_introduction_ist
	 * @param tgt_introduction_soll
	 */
	public Time(int project_id, Date start_ist, Date start_soll, Date end_ist, Date end_soll, Date nom_initial_ist, Date nom_initial_soll,
			Date nom_conception_ist, Date nom_conception_soll, Date nom_realization_ist, Date nom_realization_soll, Date nom_introduction_ist,
			Date nom_introduction_soll, Date tgt_initial_ist, Date tgt_initial_soll, Date tgt_conception_ist, Date tgt_conception_soll,
			Date tgt_realization_ist, Date tgt_realization_soll, Date tgt_introduction_ist, Date tgt_introduction_soll) {
		this.project_id = project_id;
		this.start_ist = start_ist;
		this.start_soll = start_soll;
		this.end_ist = end_ist;
		this.end_soll = end_soll;
		this.nom_initial_ist = nom_initial_ist;
		this.nom_initial_soll = nom_initial_soll;
		this.nom_conception_ist = nom_conception_ist;
		this.nom_conception_soll = nom_conception_soll;
		this.nom_realization_ist = nom_realization_ist;
		this.nom_realization_soll = nom_realization_soll;
		this.nom_introduction_ist = nom_introduction_ist;
		this.nom_introduction_soll = nom_introduction_soll;
		this.tgt_initial_ist = tgt_initial_ist;
		this.tgt_initial_soll = tgt_initial_soll;
		this.tgt_conception_ist = tgt_conception_ist;
		this.tgt_conception_soll = tgt_conception_soll;
		this.tgt_realization_ist = tgt_realization_ist;
		this.tgt_realization_soll = tgt_realization_soll;
		this.tgt_introduction_ist = tgt_introduction_ist;
		this.tgt_introduction_soll = tgt_introduction_soll;
	}
	
	/**
	 * Konstruktur mit Projekt Id wenn ein leeren Datensatz mit NULL Werten erstellt wird
	 * @param project_id
	 * @param start_soll
	 * @param end_soll
	 */
	public Time(int project_id, Date start_soll, Date end_soll) {
		this.project_id = project_id;
		this.start_soll = start_soll;
		this.end_soll = end_soll;
	}

	/**
	 * Setter und Getter Methoden
	 */
	
	public int getId() {
		return id;
	}
	
	public int getProject_id() {
		return project_id;
	}
	
	public void setProject_id(int project_id) {
		this.project_id = project_id;
	}

	public Date getStart_ist() {
		return start_ist;
	}

	public void setStart_ist(Date start_ist) {
		this.start_ist = start_ist;
	}

	public Date getStart_soll() {
		return start_soll;
	}

	public void setStart_soll(Date start_soll) {
		this.start_soll = start_soll;
	}

	public Date getEnd_ist() {
		return end_ist;
	}

	public void setEnd_ist(Date end_ist) {
		this.end_ist = end_ist;
	}

	public Date getEnd_soll() {
		return end_soll;
	}

	public void setEnd_soll(Date end_soll) {
		this.end_soll = end_soll;
	}

	public Date getNom_initial_ist() {
		return nom_initial_ist;
	}

	public void setNom_initial_ist(Date nom_initial_ist) {
		this.nom_initial_ist = nom_initial_ist;
	}

	public Date getNom_initial_soll() {
		return nom_initial_soll;
	}

	public void setNom_initial_soll(Date nom_initial_soll) {
		this.nom_initial_soll = nom_initial_soll;
	}

	public Date getNom_conception_ist() {
		return nom_conception_ist;
	}

	public void setNom_conception_ist(Date nom_conception_ist) {
		this.nom_conception_ist = nom_conception_ist;
	}

	public Date getNom_conception_soll() {
		return nom_conception_soll;
	}

	public void setNom_conception_soll(Date nom_conception_soll) {
		this.nom_conception_soll = nom_conception_soll;
	}

	public Date getNom_realization_ist() {
		return nom_realization_ist;
	}

	public void setNom_realization_ist(Date nom_realization_ist) {
		this.nom_realization_ist = nom_realization_ist;
	}

	public Date getNom_realization_soll() {
		return nom_realization_soll;
	}

	public void setNom_realization_soll(Date nom_realization_soll) {
		this.nom_realization_soll = nom_realization_soll;
	}

	public Date getNom_introduction_ist() {
		return nom_introduction_ist;
	}

	public void setNom_introduction_ist(Date nom_introduction_ist) {
		this.nom_introduction_ist = nom_introduction_ist;
	}

	public Date getNom_introduction_soll() {
		return nom_introduction_soll;
	}

	public void setNom_introduction_soll(Date nom_introduction_soll) {
		this.nom_introduction_soll = nom_introduction_soll;
	}

	public Date getTgt_initial_ist() {
		return tgt_initial_ist;
	}

	public void setTgt_initial_ist(Date tgt_initial_ist) {
		this.tgt_initial_ist = tgt_initial_ist;
	}

	public Date getTgt_initial_soll() {
		return tgt_initial_soll;
	}

	public void setTgt_initial_soll(Date tgt_initial_soll) {
		this.tgt_initial_soll = tgt_initial_soll;
	}

	public Date getTgt_conception_ist() {
		return tgt_conception_ist;
	}

	public void setTgt_conception_ist(Date tgt_conception_ist) {
		this.tgt_conception_ist = tgt_conception_ist;
	}

	public Date getTgt_conception_soll() {
		return tgt_conception_soll;
	}

	public void setTgt_conception_soll(Date tgt_conception_soll) {
		this.tgt_conception_soll = tgt_conception_soll;
	}

	public Date getTgt_realization_ist() {
		return tgt_realization_ist;
	}

	public void setTgt_realization_ist(Date tgt_realization_ist) {
		this.tgt_realization_ist = tgt_realization_ist;
	}

	public Date getTgt_realization_soll() {
		return tgt_realization_soll;
	}

	public void setTgt_realization_soll(Date tgt_realization_soll) {
		this.tgt_realization_soll = tgt_realization_soll;
	}

	public Date getTgt_introduction_ist() {
		return tgt_introduction_ist;
	}

	public void setTgt_introduction_ist(Date tgt_introduction_ist) {
		this.tgt_introduction_ist = tgt_introduction_ist;
	}

	public Date getTgt_introduction_soll() {
		return tgt_introduction_soll;
	}

	public void setTgt_introduction_soll(Date tgt_introduction_soll) {
		this.tgt_introduction_soll = tgt_introduction_soll;
	}

	/**
	 * toString Methode für die Ausgabe des ganzen Objektes mit allen Variablen
	 */
	@Override
	public String toString() {
		return "Time [id=" + id + ", project_id=" + project_id + ", start_ist=" + start_ist + ", start_soll=" + start_soll
				+ ", end_ist=" + end_ist + ", end_soll=" + end_soll + ", nom_initial_ist=" + nom_initial_ist
				+ ", nom_initial_soll=" + nom_initial_soll + ", nom_conception_ist=" + nom_conception_ist
				+ ", nom_conception_soll=" + nom_conception_soll + ", nom_realization_ist=" + nom_realization_ist
				+ ", nom_realization_soll=" + nom_realization_soll + ", nom_introduction_ist=" + nom_introduction_ist
				+ ", nom_introduction_soll=" + nom_introduction_soll + ", tgt_initial_ist=" + tgt_initial_ist
				+ ", tgt_initial_soll=" + tgt_initial_soll + ", tgt_conception_ist=" + tgt_conception_ist
				+ ", tgt_conception_soll=" + tgt_conception_soll + ", tgt_realization_ist=" + tgt_realization_ist
				+ ", tgt_realization_soll=" + tgt_realization_soll + ", tgt_introduction_ist=" + tgt_introduction_ist
				+ ", tgt_introduction_soll=" + tgt_introduction_soll + "]";
	}

}