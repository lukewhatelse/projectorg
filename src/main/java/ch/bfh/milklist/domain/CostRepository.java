package ch.bfh.milklist.domain;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

/**
 * Repository für alle Items. (Mit dem Spring JDBC-Template implementiert.)
 * Hier werden alle Operstionen für die DB-Operationen zu Items implementiert
 * @author Marcel Briggen
 *
 */
@Component()
public class CostRepository {
	
	private final Logger logger = LoggerFactory.getLogger(CostRepository.class);
	
    @Autowired
	private JdbcTemplate jdbc;
	
	
	/**
	 * SELECT: Liefert die Kosten für das jeweilige Projekt mit der übergebenen Id.
	 * Wird kein Projekt mit dieser Id gefunden, wird ein leerer Datensatz der Kosten für dieses Projekt erstellt.
	 * @param id des Projektes
	 * @return Item oder NULL
	 */
	public Cost getById(int project_id) {
		logger.trace("getById " + project_id);
		
		Cost c = new Cost();
		
		//Wenn kein Cost Objekt vorhanden ist, wir ein leeres angelegt und in die DB gefüllt
		if(jdbc.query("SELECT * FROM cost WHERE project_id=?",new Object [] {project_id}, new CostMapper()).isEmpty()){
			c = new Cost(project_id, (double)0, (double)0, (double)0, (double)0, (double)0, (double)0, (double)0, (double)0);
			this.insert(c);
		}else{
			c = jdbc.queryForObject("SELECT * FROM cost WHERE project_id=?",new Object [] {project_id}, new CostMapper());
		}
		
		return c;
	}
	
	
	/**
	 * SELECT: Liefert Projekt Status für das jeweilige Projekt.
	 * @param project_id
	 * @return s
	 */
	public State getProgressById(int project_id) {
		logger.trace("getProgressByProjectId " + project_id);
		
		State s = new State();
		
		if (jdbc.query("SELECT progress FROM state WHERE project_id=? ORDER BY id DESC LIMIT 1",new Object[] {project_id}, new ProgressMapper()).isEmpty()){
			s = new State(project_id, null, null, (int)0, (int)0, (int)0, (int)0, (int)0, null);
			this.insertProgress(s);
		}else{
			s = jdbc.queryForObject("SELECT progress FROM state WHERE project_id=? ORDER BY id DESC LIMIT 1",new Object[] {project_id}, new ProgressMapper());
		}
		
		return s;
	}
	
	
	/**
	 * INSERT: Speichert das angegebene Item in der Datenbank.
	 * @param i neu zu erstellendes Item
	 * @return Liefert die von der DB generierte id des neuen Items zurück
	 */
	public Integer insert(Cost c) {
		logger.trace("insert " + c);
		jdbc.update("INSERT INTO cost (project_id, nom_initial, nom_conception, nom_realization, nom_introduction, tgt_initial, tgt_conception,"
				+ " tgt_realization, tgt_introduction) VALUES (?,?,?,?,?,?,?,?,?)",c.getProject_id(),c.getNom_initial(),c.getNom_conception(),
				c.getNom_realization(),c.getNom_introduction(),c.getTgt_initial(),c.getTgt_conception(),c.getTgt_realization(),c.getTgt_introduction());
		Integer id = jdbc.queryForObject("select IDENTITY();", Integer.class);

		return id;
	}
	
	
	/**
	 * INSERT: Speichert das angegebene Item in die Tabelle state.
	 * @param s
	 * @return id
	 */
	public Integer insertProgress(State s) {
		logger.trace("insertProgress " + s);
		jdbc.update("INSERT INTO state (project_id, period_start, period_end, cost, date, ressource, risk, progress, comment)"
		+ " VALUES (?,?,?,?,?,?,?,?,?)",s.getProject_id(),s.getPeriod_start(),s.getPeriod_end(),s.getCost(),s.getDate(),
		s.getRessource(),s.getRisk(),s.getProgress(),s.getComment());
		Integer id = jdbc.queryForObject("select IDENTITY();", Integer.class);

		return id;
	}
	

	/**
	 * UPDATE: Speichert das übergebene item in der Datenbank.
	 * @param c
	 */
	public void save(Cost c) {
		logger.trace("save " + c);
		jdbc.update("UPDATE cost SET nom_initial=?, nom_conception=?, nom_realization=?, nom_introduction=?, tgt_initial=?, tgt_conception=?,"
				+ " tgt_realization=?, tgt_introduction=? WHERE project_id=?",c.getNom_initial(),c.getNom_conception(),c.getNom_realization(),
				c.getNom_introduction(),c.getTgt_initial(),c.getTgt_conception(),c.getTgt_realization(),c.getTgt_introduction(),c.getProject_id());
	}


	/**
	 * DELETE: Löscht das Item mit der angegebenen Id von der Datenbank.
	 * @param id Item ID
	 */
	public void delete(Integer id) {
		logger.trace("delete cost with id=" + id);
		jdbc.update("DELETE FROM cost WHERE id=?",id);
	}

	
	/**
	 * Hilfsklasse zum Konvertieren der Resultsets in Item-Objekte. Siehe getAll & getById Methoden.
	 * @author Marcel Brigen
	 */
	private class CostMapper implements RowMapper<Cost>
	{
		@Override
		public Cost mapRow(ResultSet rs, int rownum) throws SQLException {
			Cost c = new Cost(rs.getInt("id"),rs.getInt("project_id"),rs.getDouble("nom_initial"),rs.getDouble("nom_conception"),
					rs.getDouble("nom_realization"),rs.getDouble("nom_introduction"),rs.getDouble("tgt_initial"),rs.getDouble("tgt_conception")
					,rs.getDouble("tgt_realization"),rs.getDouble("tgt_introduction"));
			return c;
		}
	}
	
	
	/**
	 * Hilfsklasse zum Konvertieren der Resultsets in Item-Objekte für den Projektstatus. Siehe getProgressByProjectId Methoden.
	 */
	private class ProgressMapper implements RowMapper<State>
	{
		@Override
		public State mapRow(ResultSet rs, int rownum) throws SQLException {
			State s = new State(rs.getInt("progress"));
			return s;
		}
	}

}