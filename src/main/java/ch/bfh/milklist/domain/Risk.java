package ch.bfh.milklist.domain;

import java.sql.Date;

/**
 * Einzelner Eintrag in der Liste mit Name und Menge (und einer eindeutigen Id)
 * @author Marcel Briggen
 *
 */
public class Risk {
	private int id;
	private int project_id;
	private String decription;
	private String measures;
	private float etw;
	private String etw_text;
	private int impact;
	private int critical;
	private String impact_text;
	private String critical_text;
	private String responsible_firstname;
	private String responsible_lastname;
	private int responsible_id;
	private String responsible;
	private String state_phase;
	private Date last_edited;
	
	/**
	 * Defaultkonstruktor für die Verwendung in einem Controller
	 */
	public Risk()
	{
		
	}
	
	/**
	 * Konstruktor für leeres Objekt, an welchem die projekt ID übergeben wird.
	 * @param project_id
	 */
	public Risk(int project_id) {
		this.project_id = project_id;
	}

	/**
	 * Konstruktor mit responsible_fistname und responsible_lastname
	 * @param id
	 * @param project_id
	 * @param decription
	 * @param measures
	 * @param etw
	 * @param impact
	 * @param critical
	 * @param responsible_firstname
	 * @param responsible_lastname
	 * @param responsible_id
	 * @param state_phase
	 * @param last_edited
	 */
	public Risk(int id, int project_id, String decription, String measures, float etw, int impact, int critical,
			String responsible_firstname, String responsible_lastname, int responsible_id, String state_phase, Date last_edited) {
		this.id = id;
		this.project_id = project_id;
		this.decription = decription;
		this.measures = measures;
		this.etw = etw;
		this.impact = impact;
		this.critical = critical;
		this.responsible_firstname = responsible_firstname;
		this.responsible_lastname = responsible_lastname;
		this.responsible_id = responsible_id;
		this.state_phase = state_phase;
		this.last_edited = last_edited;
		
		this.responsible = responsible_firstname + " " + responsible_lastname;
		this.impact_text = transformToText(impact);
		this.critical_text = transformToText(critical);
		this.etw_text = etw + "%";
	}

	/**
	 * Konstruktor ohne responsible_fistname und responsible_lastname
	 * @param id
	 * @param project_id
	 * @param decription
	 * @param measures
	 * @param etw
	 * @param impact
	 * @param critical
	 * @param responsible_id
	 * @param state_phase
	 * @param last_edited
	 */
	public Risk(int id, int project_id, String decription, String measures, float etw, int impact, int critical,
			int responsible_id, String state_phase, Date last_edited) {
		this.id = id;
		this.project_id = project_id;
		this.decription = decription;
		this.measures = measures;
		this.etw = etw;
		this.impact = impact;
		this.critical = critical;
		this.responsible_id = responsible_id;
		this.state_phase = state_phase;
		this.last_edited = last_edited;
		
		this.impact_text = transformToText(impact);
		this.critical_text = transformToText(critical);
		this.etw_text = etw + "%";
	}

	/**
	 * Setter und Getter Methoden
	 */
	
	public int getId() {
		return id;
	}

	public int getProject_id() {
		return project_id;
	}

	public void setProject_id(int project_id) {
		this.project_id = project_id;
	}

	public String getDecription() {
		return decription;
	}

	public void setDecription(String decription) {
		this.decription = decription;
	}

	public String getMeasures() {
		return measures;
	}

	public void setMeasures(String measures) {
		this.measures = measures;
	}

	public float getEtw() {
		return etw;
	}

	public void setEtw(float etw) {
		this.etw = etw;
	}

	public String getEtw_text() {
		return etw_text;
	}

	public int getImpact() {
		return impact;
	}

	public void setImpact(int impact) {
		this.impact = impact;
	}

	public int getCritical() {
		return critical;
	}

	public void setCritical(int critical) {
		this.critical = critical;
	}

	public String getImpact_text() {
		return impact_text;
	}

	public void setImpact_text(String impact_text) {
		this.impact_text = impact_text;
	}

	public String getCritical_text() {
		return critical_text;
	}

	public void setCritical_text(String critical_text) {
		this.critical_text = critical_text;
	}

	public int getResponsible_id() {
		return responsible_id;
	}

	public void setResponsible_id(int responsible_id) {
		this.responsible_id = responsible_id;
	}

	public String getResponsible() {
		return responsible;
	}

	public void setResponsible(String responsible) {
		this.responsible = responsible;
	}

	public String getState_phase() {
		return state_phase;
	}

	public void setState_phase(String state_phase) {
		this.state_phase = state_phase;
	}

	public Date getLast_edited() {
		return last_edited;
	}

	public void setLast_edited(Date last_edited) {
		this.last_edited = last_edited;
	}
	
	/**
	 * Methode welche die Kritikalität/Auswirkung von Nummern in Text umwandelt
	 * @param number
	 */
	public String transformToText(int number) {
		String textToReturn;
		if (number == 1) {
			textToReturn = "leicht";
		} else if (number == 2) {
			textToReturn = "mittel";
		} else if (number == 3) {
			textToReturn = "schwer";
		} else {
			textToReturn = null;
		}
		
		return textToReturn;
	}

	/**
	 * toString Methode für die Ausgabe des ganzen Objektes mit allen Variablen
	 */
	@Override
	public String toString() {
		return "Risk [id=" + id + ", project_id=" + project_id + ", decription=" + decription + ", measures="
				+ measures + ", etw=" + etw + ", impact=" + impact + ", critical=" + critical + ", responsible="
				+ responsible + ", responsible_id=" + responsible_id + ", state_phase=" + state_phase + ", last_edited="
				+ last_edited + "]";
	}
}
