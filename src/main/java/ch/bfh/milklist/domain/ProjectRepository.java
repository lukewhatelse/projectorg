package ch.bfh.milklist.domain;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

/**
 * Repository für alle Items. (Mit dem Spring JDBC-Template implementiert.)
 * Hier werden alle Operstionen für die DB-Operationen zu Items implementiert
 * @author Marcel Briggen
 *
 */
@Component() 
public class ProjectRepository {
	
	private final Logger logger = LoggerFactory.getLogger(ProjectRepository.class);
	
    @Autowired
	private JdbcTemplate jdbc;

	/**
	 * SELECT: Liefert alle items in der Datenbank.
	 * @return Collection aller Items
	 */
	public Collection<Project> getAll()  {
		logger.trace("getAll");
		return jdbc.query("SELECT p.id, p.name, p.start, p.end, p.phase, c.firstname AS customer_firstname,"
				+ " c.lastname AS customer_lastname, l.firstname AS leader_firstname, l.lastname AS leader_lastname, p.customer_id, p.projectleader_id"
				+ " FROM project AS p"
				+ " INNER JOIN people AS c"
				+ " ON p.customer_id = c.id"
				+ " INNER JOIN people AS l"
				+ " ON p.projectleader_id = l.id", new ProjectMapper());
	}

	
	/**
	 * SELECT: Liefert das Item mit der übergebenen Id
	 * @param id id des Item
	 * @return Item oder NULL
	 */
	public Project getById(int id) {
		logger.trace("getById " + id);
		return jdbc.queryForObject("SELECT p.id, p.name, p.start, p.end, p.phase, c.firstname AS customer_firstname,"
				+ " c.lastname AS customer_lastname, l.firstname AS leader_firstname, l.lastname AS leader_lastname, p.customer_id, p.projectleader_id"
				+ " FROM project AS p"
				+ " INNER JOIN people AS c"
				+ " ON p.customer_id = c.id"
				+ " INNER JOIN people AS l"
				+ " ON p.projectleader_id = l.id"
				+ " WHERE p.id=?",new Object [] {id}, new ProjectMapper());
	}

	
	/**
	 * INSERT: Speichert das angegebene Item in der Datenbank.
	 * @param i neu zu erstellendes Item
	 * @return Liefert die von der DB generierte id des neuen Items zurück
	 */
	public Integer insert(Project i) {
		logger.trace("insert " + i);
		jdbc.update("INSERT INTO project (name, start, end, phase, projectleader_id, customer_id) values (?,?,?,?,?,?)",
				i.getName(),i.getStart(),i.getEnd(),i.getProject_phase(), i.getProjectleader_id(), i.getCustomer_id());
		Integer id = jdbc.queryForObject("select IDENTITY();", Integer.class);
		
		return id;
	}
	
	
	/**
	 * UPDATE: Speichert das übergebene item in der Datenbank.
	 * @param i
	 */
	public void save(Project i) {
		logger.trace("save " + i);
		jdbc.update("update project set name=?, start=?, end=?, phase=?, customer_id=?, projectleader_id=? where id=?",
				i.getName(),i.getStart(),i.getEnd(),i.getProject_phase(), i.getCustomer_id(), i.getProjectleader_id() ,i.getId());
	}

	
	/**
	 * DELETE: Löscht das Item mit der angegebenen Id aus der Datenbank.
	 * @param id Item ID
	 */
	public void delete(Integer id) {
		logger.trace("delete project with id=" + id);
		
		// Löscht zuerst die Daten mit Referenzen auf dieses Projekt
		jdbc.update("DELETE FROM cost WHERE project_id=?", id);
		jdbc.update("DELETE FROM risk WHERE project_id=?", id);
		jdbc.update("DELETE FROM state WHERE project_id=?", id);
		jdbc.update("DELETE FROM time WHERE project_id=?", id);
		// Löscht das Projekt aus der Datenbank
		jdbc.update("DELETE FROM project WHERE id=?",id);
	}

	
	/**
	 * Hilfsklasse zum Konvertieren der Resultsets in Item-Objekte. Siehe getAll, getById & getByName Methoden.
	 * @author Marcel Brigen
	 *
	 */
	private class ProjectMapper implements RowMapper<Project>
	{
		@Override
		public Project mapRow(ResultSet rs, int rownum) throws SQLException {
			Project i = new Project(rs.getInt("id"),rs.getString("name"),rs.getDate("start"),rs.getDate("end"),rs.getString("phase"),
					rs.getInt("customer_id"),rs.getInt("projectleader_id"),rs.getString("customer_firstname") + " " + rs.getString("customer_lastname"),
					rs.getString("leader_firstname") + " " + rs.getString("leader_lastname"));
			return i;
		}
	}
	
}