package ch.bfh.milklist;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;

/**
 * Spring Boot Mainklasse und Konfigruation. DO NOT EDIT.
 * @author Marcel Briggen
 */
@SpringBootApplication
public class Application {
	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}
	
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
	  if (!registry.hasMappingForPattern("/css/**")) {
	     registry.addResourceHandler("/css/**").addResourceLocations("classpath:/css/");
	     registry.addResourceHandler("/script/**").addResourceLocations("classpath:/script/");
	  }
	}

}
