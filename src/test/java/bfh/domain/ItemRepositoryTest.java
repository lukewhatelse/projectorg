package bfh.domain;

import static org.junit.Assert.*;
import static org.hamcrest.Matchers.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import ch.bfh.milklist.Application;
import ch.bfh.milklist.domain.Project;
import ch.bfh.milklist.domain.ProjectRepository;

@TestPropertySource("classpath:application-test.properties")
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes=Application.class)
public class ItemRepositoryTest {

	@Autowired
	ProjectRepository itemRepo;
	
	@Autowired
	private JdbcTemplate jdbc;
	
	//Werte für die Testinitialisierung
	static final String ORIGINAL_NAME_1 = "ERP Migration";
	static final String ORIGINAL_START_1 = "2016-03-14";
	static final String ORIGINAL_END_1 = "2016-12-31";
	static final String ORIGINAL_PHASE_1 = "Initialisierung";
	int ORIGINAL_ID;
	
	/**
	 * Fügt ein paar Daten direkt via JDBC in die DB ein.
	 */
	@Before
	public void setUp(){
		jdbc.update("insert into project (name, start, end, phase) values (?,?,?,?)",ORIGINAL_NAME_1,ORIGINAL_START_1,ORIGINAL_END_1,ORIGINAL_PHASE_1);
		ORIGINAL_ID = jdbc.queryForObject("select IDENTITY();", int.class);
	}
	
	
	/**
	 *Löscht die Items Tabelle
	 */
	@After
	public void tearDown()
	{
		jdbc.update("delete from project");
	}
	
	/**
	 * Insert eines Items, danach alle auslesen und prüfen ob das Objekt mit der zurückgelieferten ID auch tatsächlich die richtigen Werte hat.
	 */
	
	/*
	@Test
	public void testInsert() {
		long id = itemRepo.insert(new Item(0,"testInsert", 2016-03-14, 2016-12-31,"ddd", "Robin", "Csontos", "Lucas", "Badertscher"));
		Collection<Item> itemList = itemRepo.getAll();
		for(Item i:itemList)
		{
			if(i.getId()==id)
			{
				assertThat(i.getProject_phase(),is("ddd"));
			}
		}
	}
	*/
	/**
	 * Update eines Objekts und danach prüfen ob es die richtigen Werte hat
	 */
	@Test
	public void testUpdate() {
		
		final String NEW_NAME = "testUpdate2";
		final String NEW_PHASE = ORIGINAL_PHASE_1 + "Realisierung";
		
		
		Project newOne = itemRepo.getById(ORIGINAL_ID);
		newOne.setName(NEW_NAME);
		newOne.setProject_phase(NEW_PHASE);
		itemRepo.save(newOne);
		
		Project updatedOne = itemRepo.getById(ORIGINAL_ID);
		assertThat(updatedOne.getName(), is(NEW_NAME));
		assertThat(updatedOne.getProject_phase(), is(NEW_PHASE));
	}
	
	

}
